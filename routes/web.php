<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Route::get('/parselist', 'Parse@carsList');
//Route::get('/parsemodels', 'Parse@carModels');
//Route::get('/parsecomplectations', 'Parse@parseComplectations');
//Route::get('/merge', 'Parse@merge');
//Route::get('/drom', 'Parse@parseDrom');
//Route::get('/updatemodels', 'Parse@updateModels');

Route::get('/parseferio', 'Parse@parseFerio');

Route::get('/parseautoru', 'Parse@parseAutoRu');
Route::get('/autodrom', 'Parse@autodrom');
Route::get('/boxvalues', 'Parse@addBoxValues');
Route::get('/toelastic', 'Parse@toElastic');
Route::post('/get-models', 'HomeController@getModels');
Route::post('/get-results', 'HomeController@getResults');
Route::post('/get-input-results', 'HomeController@getInputResults');


Route::get('/crm/update-deal/{id}', 'AmoCrmController@updateDeal');
Route::get('/crm/update-matching', 'AmoCrmController@updateFieldMatching');
Route::get('/crm/add-note/{id}', 'AmoCrmController@addNoteToOrder');
Route::get('/crm/set-lead/{orderId}', 'AmoCrmController@setOrderId');
Route::get('/crm/create-deal/{number}/{carId}/{year}', 'AmoCrmController@createDeal');

Route::get('/ferio/send/', 'AmoCrmController@sendFerio');

Route::get('sms/test', 'AmoCrmController@smsTest');

Route::group([
    'middleware' => ['auth'],
], function ($router) {
    Route::post('/edit-car', 'HomeController@editCar');
    Route::post('/change-base', 'HomeController@changeBase');
    Route::get('/admin', 'HomeController@admin')->name('admin');
    Route::any('/api-settings', 'ProfileController@editApiSettings')->name('editApiSettings');

});

Route::group([
    'middleware' => ['cors'],
], function ($router) {
    Route::get('/models/{mark}', 'ApiController@getMarkModels');
});

Auth::routes();

Route::get('/', 'HomeController@client')->name('client');


