<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarComplectationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('car_complectations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('generation_id');
            $table->string('model_name');
            $table->string('volume');
            $table->string('horse_power');
            $table->string('engine_type');
            $table->string('box_type');
            $table->string('unit_type');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('car_complectations');
    }
}
