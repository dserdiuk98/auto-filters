<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cars', function (Blueprint $table) {
            $table->increments('id');
            $table->string('mark');
            $table->string('model');
            $table->string('body_type');
            $table->string('body_code');
            $table->integer('year_from');
            $table->integer('year_to');
            $table->string('model_name');
            $table->float('volume');
            $table->integer('horse_power');
            $table->string('engine_type');
            $table->string('engine');
            $table->string('box_type');
            $table->string('unit_type');
            $table->string('url');
            $table->string('url_drom');
            $table->string('market');
            $table->string('parse_type');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cars');
    }
}
