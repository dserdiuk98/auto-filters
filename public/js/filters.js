// register the component
Vue.component('treeselect', VueTreeselect.Treeselect);
Vue.component('treeselect_serie', VueTreeselect.Treeselect);

new Vue({
    el: '#app',
    data: {
        // define default value
        value: null,
        value1: null,
        // define options
        options: [ {
            id: 'a',
            label: 'BMW',
        }, {
            id: 'b',
            label: 'AUDI',
        }, {
            id: 'c',
            label: 'Mercedes',
        } ],
        options1: [
            {
                'id': '0',
                'label': 'Любая'
            },
            {
            id: 'a',
            label: 'Автомат',
            children: [ {
                id: 'aa',
                label: 'Автоматическая',
            }, {
                id: 'ab',
                label: 'Робот',
            }, {
                id: 'abс',
                label: 'Вариатор',
            } ],
        }, {
            id: 'b',
            label: 'Механическая',
        } ],
    },
});