<?php

namespace App\Http\Controllers;

use App\Cars;
use Illuminate\Http\Request;

class ApiController extends Controller
{
    public function getMarkModels ($mark) {
        $modelList = [];
        $models = (new Cars())->where('mark', $mark)->select('model')->groupBy('model')->get();
        foreach ($models as $model) {
            $modelList[] = $model->model;
        }
        return $modelList;
    }
}
