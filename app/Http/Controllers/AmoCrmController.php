<?php

namespace App\Http\Controllers;

use AmoCRM\Client;
use App\Cars;
use App\FieldMatching;
use App\Services\AmoCrmService;
use App\Services\FerioService;
use App\Services\NumberService;
use App\Services\SmsService;
use Facebook\WebDriver\Chrome\ChromeOptions;
use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\WebDriverBy;
use Facebook\WebDriver\WebDriverExpectedCondition;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;

class AmoCrmController extends Controller
{
    protected $fieldMatching = [
        'mark' => 'enums',
    ];

    public function setOrderId ($orderId) {
        return redirect(route('admin'))->cookie('orderId', $orderId, 0);
    }

    public function addNoteToOrder ($carId) {
        $car = (new Cars())->where('id', $carId)->first();
        $amo = new AmoCrmService();
        $carFields = [
            $car->mark,
            $car->model,
            $car->body_type,
            $car->body_code,
            $car->year_from . ' - ' . $car->year_to,
            $car->model_name,
            $car->volume,
            $car->horse_power . 'л.с.',
            $car->engine_type,
            $car->engine,
            $car->box_type,
            $car->unit_type,
            $car->market
        ];
        $amo->addNoteToDeal(implode(' ', $carFields), Cookie::get('orderId'));
    }

    public function updateFieldMatching () {
        FieldMatching::truncate();
        $amo = new AmoCrmService();
        $fieldList = $amo->fieldsList()->leads;

        $fieldWithOptions = [
            'Марка',
            'Объем',
            'Тип двигателя',
            'Привод',
            'Тип кузова',
            'КПП',
        ];
        $fieldWithoutOptions = [
            'Модель',
            'Мощность',
            'Код кузова',
            'Код двигателя',
            'Период выпуска',
            'Рынок',
        ];

        foreach ($fieldList as $field) {

            if (in_array($field->name, $fieldWithOptions) && isset($field->enums)) {
                if ($field->name == 'Марка') {
                    $fieldName = 'mark';
                } else if ($field->name == 'Объем') {
                    $fieldName = 'volume';
                } else if ($field->name == 'Тип двигателя') {
                    $fieldName = 'engine_type';
                } else if ($field->name == 'Привод') {
                    $fieldName = 'unit_type';
                } else if ($field->name == 'Тип кузова') {
                    $fieldName = 'body_type';
                } else if ($field->name == 'КПП') {
                    $fieldName = 'box_type';
                }
                $fileMatch = new FieldMatching();
                $fileMatch->field = $fieldName;
                $fileMatch->crm_field = $field->id;
                $fileMatch->type = 'field';
                $fileMatch->save();
                foreach ($field->enums as $optionKey => $value) {
                    $fieldMatch = new FieldMatching();
                    $fieldMatch->field = $value;
                    $fieldMatch->crm_field = $optionKey;
                    $fieldMatch->type = $fieldName;
                    $fieldMatch->save();
                }
            }
            if (in_array($field->name, $fieldWithoutOptions)) {
                if ($field->name == 'Модель') {
                    $fieldName = 'model';
                } else if ($field->name == 'Мощность') {
                    $fieldName = 'horse_power';
                } else if ($field->name == 'Код кузова') {
                    $fieldName = 'body_code';
                } else if ($field->name == 'Код двигателя') {
                    $fieldName = 'engine';
                } else if ($field->name == 'Период выпуска') {
                    $fieldName = 'year';
                } else if ($field->name == 'Рынок') {
                    $fieldName = 'market';
                }
                $fileMatch = new FieldMatching();
                $fileMatch->field = $fieldName;
                $fileMatch->crm_field = $field->id;
                $fileMatch->type = 'field';
                $fileMatch->save();
            }
        }
    }

    public static function fieldsToLead($lead, $carId, $addParts = false) {
        $fieldWithOptions = [
            'mark',
            'volume',
            'engine_type',
            'unit_type',
            'body_type',
            'box_type',
        ];
        $fieldWithoutOptions = [
            'model',
            'horse_power',
            'body_code',
            'engine',
            'market',
        ];

        $car = (new Cars())->where('id', $carId)->first()->toArray();
        foreach ($car as $fieldName => $fieldValue) {
            if (in_array($fieldName, $fieldWithOptions)) {
                $mainFieldMatching = (new FieldMatching())
                    ->where('type', 'field')->where('field', $fieldName)->first();
                $fieldMatching = (new FieldMatching())
                    ->where('type', $fieldName)->where('field', $fieldValue)->first();
                if ($fieldMatching && $mainFieldMatching) {
                    $lead->addCustomField($mainFieldMatching->crm_field, $fieldMatching->crm_field);
                }
            } else if (in_array($fieldName, $fieldWithoutOptions)) {
                $fieldMatching = (new FieldMatching())
                    ->where('type', 'field')->where('field', $fieldName)->first();
                if ($fieldMatching) {
                    $lead->addCustomField($fieldMatching->crm_field, $fieldValue);
                }
            } else if ($fieldName == 'year_from') {
                $fieldMatching = (new FieldMatching())
                    ->where('type', 'field')->where('field', 'year')->first();
                if ($fieldMatching) {
                    $lead->addCustomField($fieldMatching->crm_field, $car['year_from'] . '-' . $car['year_to']);
                }
            }
        }

        if ($addParts) {
            if ($car['box_type'] === 'автомат') {
                $boxType = 'АКПП';
            } elseif ($car['box_type'] === 'механика') {
                $boxType = 'МКПП';
            } else {
                $boxType = $car['box_type'];
            }
            $lead['name'] = $car['mark'] . " " . $car['model'];
            $lead->addCustomField(1757498, $boxType);
            $lead->addCustomField(1757502, $car['box_val'] . " ст. " . $car['unit_type'] . " привод для двигателя " . $car['engine']);
        }

        return $lead;
    }

    public function updateDeal ($carId) {
        $client = new Client(env('CRM_SUBDOMAIN'), Auth::user()->crm_email, Auth::user()->crm_key);
        $lead = $client->lead;
        self::fieldsToLead($lead, $carId);
        $lead->apiUpdate(Cookie::get('orderId'), 'now');
    }

    public function sendText ($text) {
        $amo = new AmoCrmService();
        $amo->addNoteToDeal($text, Cookie::get('orderId'));
    }

    public function createDeal ($number, $carId, $year) {
        $client = new Client(env('CRM_SUBDOMAIN'), 'allregdata@yandex.ru', '1009ec0720aeccca0cad92f92e418a163ae892a2');
        $lead = $client->lead;
        $lead['status_id'] = (int)24528276;
        $lead->addCustomField(1747252, $year);
        self::fieldsToLead($lead, $carId, true);
        $dealId = $lead->apiAdd();
        $contact = $client->contact;
        $number = NumberService::phone($number);
        $contacts = $contact->apiList(['query' => $number]);
        if (count($contacts) > 0) {
            $contactId = $contacts[key($contacts)]['id'];
            $contact['linked_leads_id'] = $dealId;
            $contact->apiUpdate((int)$contactId);
        } else {
            $contact['name'] = $number;
            $contact['linked_leads_id'] = $dealId;
            $contact->addCustomField(1427400, [
                [$number, 'WORK'],
            ]);
            $contact->apiAdd();
        }

        sleep(30);
        FerioService::sendToFerio((int)$dealId);
    }

    public function sendFerio () {
        FerioService::sendToFerio(49466404);
    }

    public function smsTest()
    {
        $client = new Client(env('CRM_SUBDOMAIN'), 'allregdata@yandex.ru', '1009ec0720aeccca0cad92f92e418a163ae892a2');
        $leads = $client->lead->apiList([
            'status' => 24528276
        ]);
        if (count($leads) > 0) {
            foreach ($leads as $lead) {
                if ($lead['date_create'] + 60 * 60 * 2 > time()) {
                    continue;
                }

                $leadId = $lead['id'];
                foreach ($lead['custom_fields'] as $customField) {
                    if ($customField['id'] == 1757498) {
                        $customFieldBox = $customField['values'][0]['value'];
                    }
                    if ($customField['id'] == 1757774) {
                        $customFieldValue = $customField['values'][0]['value'];
                        preg_match('/order_id=([0-9]+)/', $customFieldValue, $matches);
                        $orderId = $matches[1];
                        $host = 'http://82.146.39.128:4444/wd/hub';
                        $options = new ChromeOptions();
                        $options->addArguments(array('--no-sandbox'));
                        $capabilities = DesiredCapabilities::chrome();
                        $capabilities->setCapability(ChromeOptions::CAPABILITY, $options);
                        $driver = RemoteWebDriver::create($host, $capabilities);
                        $driver->get('https://i.ferio.ru/');
                        $driver->findElement(WebDriverBy::name('email'))->sendKeys('mr.maxego@gmail.com');
                        $driver->findElement(WebDriverBy::name('password'))->sendKeys('Dd12345678');
                        $driver->findElement(WebDriverBy::tagName('button'))->click();
                        $driver->wait()->until(
                            WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::id('mainmenu'))
                        );
                        $driver->get('http://partner.ferio.ru/RequestMgt/ShowAnswerByParts.php?order_id=' . $orderId);
                        $content = $driver->getPageSource();
                        preg_match_all('|<td class="price">(.*?)руб.<\/td>|is', $content, $matches);
                        if (count($matches[1]) > 2) {
                            $minPrice = 99999;
                            $maxPrice = -99999;
                            foreach ($matches[1] as $price) {
                                $Price = (int)str_replace('&nbsp;', '', $price);
                                if ($Price < $minPrice) {
                                    $minPrice = $Price;
                                }
                                if ($Price > $maxPrice) {
                                    $maxPrice = $Price;
                                }
                            }
                            $minPrice = $minPrice * 1000 + 10000;
                            $maxPrice = $maxPrice * 1000 + 10000;
                            $rand = rand(4, 8);
                            $name = $lead['name'];

                            $contact = $client->contact->apiList([
                                'id' => $lead['main_contact_id']
                            ]);

                            $number = 0;
                            if (count($contact) > 0) {
                                $number = NumberService::getNumberFromContact($contact[0]);
                            }

                            $message = "{$name}. {$customFieldBox} от {$minPrice}руб до {$maxPrice}руб (цена зависит от пробега и маркировки). В наличии {$rand} шт. По РФ не ездили.";
                            SmsService::sendSms($message, $number);
                            $uLead = $client->lead;
                            $uLead['status_id'] = (int)24528306;
                            $prices = "{$minPrice}.{$maxPrice}";
                            $uLead->apiUpdate((int)$leadId, 'now');
                            $amo = new AmoCrmService('allregdata@yandex.ru', '1009ec0720aeccca0cad92f92e418a163ae892a2');
                            $amo->addNoteToDeal($message, (int)$leadId);
                            echo $prices;
                        } else {
                            $uLead = $client->lead;
                            $uLead['status_id'] = 24528279;
                            $uLead->apiUpdate((int)$leadId, 'now');
                        }

                        $driver->close();
                    }
                }
            }
        }
    }
}
