<?php

namespace App\Http\Controllers;

use App\CarComplectations;
use App\carGenerations;
use App\Cars;
use App\carsDrom;
use App\CarsUrl;
use App\Services\ParseFerioService;
use Elasticsearch\ClientBuilder;
use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\WebDriverBy;
use Facebook\WebDriver\WebDriverExpectedCondition;
use GuzzleHttp\Client;
use GuzzleHttp\Cookie\CookieJar;
use function GuzzleHttp\Promise\all;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Parse extends Controller
{

    public function parseFerio () {
        set_time_limit(0);
        ParseFerioService::parseAll();
    }


    public function updateModels () {
        $modelList =
            array (
                '1-Series' => '1 серия',
                '2-Series' => '2 серия',
                '2-Series Active Tourer' => '2 серия Active Tourer',
                '2-Series Grand Tourer' => '2 серия Grand Tourer',
                '3-Series' => '3 серия',
                '4-Series' => '4 серия',
                '488 Pista' => 488.0,
                '5-Series' => '5 серия',
                '6-Series' => '6 серия',
                '7-Series' => '7 серия',
                '8-Series' => '8 серия',
                'A-Class' => 'A-klasse',
                'A4 allroad quattro' => 'A4 allroad',
                'A6 allroad quattro' => 'A6 allroad',
                'Acty Truck' => 'Acty',
                'Amulet A15' => 'Amulet (A15)',
                'B-Class' => 'B-klasse',
                'B9 Tribeca' => 'Tribeca',
                'Be-Go' => 'Be-go',
                'Bonus 3 - A19' => 'Bonus 3 (E3/A19)',
                'Breez' => 'Breez (520)',
                'C-Class' => 'C-klasse',
                'cee\'d' => 'Ceed',
                'CL-Class' => 'CL-klasse',
                'CLA-Class' => 'CLA-klasse',
                'CLK-Class' => 'CLK-klasse',
                'CLS-Class' => 'CLS-klasse',
                'Corona Exiv' => 'Corona EXiV',
                'DAYZ' => 'Dayz',
                'E-Class' => 'E-klasse',
                'ek Custom' => 'eK Custom',
                'eK-Sport' => 'eK Sport',
                'eK-Wagon' => 'eK Wagon',
                'ES200' => 'ES',
                'ES250' => 'ES',
                'ES300' => 'ES',
                'ES300h' => 'ES',
                'ES350' => 'ES',
                'Estima Emina' => 'Estima',
                'Estima Lucida' => 'Estima',
                'F150' => 'F-150',
                'Familia S-Wagon' => 'Familia',
                'Freed Spike' => 'Freed',
                'Funcargo' => 'FunCargo',
                'FX30d' => 'FX',
                'FX35' => 'FX',
                'FX37' => 'FX',
                'FX45' => 'FX',
                'FX50' => 'FX',
                'G-Class' => 'G-klasse',
                'G35' => 'G',
                'G37' => 'G',
                'GL-Class' => 'GL-klasse',
                'GLA-Class' => 'GLA-klasse',
                'GLK-Class' => 'GLK-klasse',
                'Grand C4 Picasso' => 'C4 Picasso',
                'Grand Vitara XL-7' => 'Grand Vitara',
                'Grand Voyager' => 'Voyager',
                'GS250' => 'GS',
                'GS300' => 'GS',
                'GS300h' => 'GS',
                'GS350' => 'GS',
                'GS430' => 'GS',
                'GS450h' => 'GS',
                'GS460' => 'GS',
                'GT 86' => 'GT86',
                'GX460' => 'GX',
                'GX470' => 'GX',
                'Hiace' => 'HiAce',
                'HS250h' => 'HS',
                'IS200t' => 'IS',
                'IS250' => 'IS',
                'IS250C' => 'IS',
                'IS300' => 'IS',
                'IS300h' => 'IS',
                'IS350' => 'IS',
                'IS350C' => 'IS',
                'Isis' => 'ISis',
                'ist' => 'Ist',
                'Kluger V' => 'Kluger',
                'LC500' => 'LC',
                'LS400' => 'LS',
                'LS430' => 'LS',
                'LS460' => 'LS',
                'LS460L' => 'LS',
                'LS500' => 'LS',
                'LS600h' => 'LS',
                'LS600hL' => 'LS',
                'LX470' => 'LX',
                'LX570' => 'LX',
                'M-Class' => 'C-klasse',
                'M11' => 'M11 (A3)',
                'Mark X Zio' => 'Mark X ZiO',
                'Max' => 'MAX',
                'Mazda2' => 2.0,
                'Mazda3' => 3.0,
                'Mazda5' => 5.0,
                'Mazda6' => 6.0,
                'Mohave' => 'Mohave (Borrego)',
                'N-BOX+' => 'N-BOX',
                'N-ONE' => 'N-One',
                'Navara' => 'Navara (Frontier)',
                'NV350 Caravan' => 'NV350 Caravan',
                'NX200' => 'NX',
                'NX200t' => 'NX',
                'NX300' => 'NX',
                'NX300h' => 'NX',
                'Pleo Plus' => 'Pleo',
                'Prius C' => 'Prius c',
                'Prius v' => 'Prius v (+)',
                'R-Class' => 'R-klasse',
                'Ram' => 'RAM',
                'RAV4' => 'RAV 4',
                'RC200t' => 'RC',
                'RC300' => 'RC',
                'RC300h' => 'RC',
                'RC350' => 'RC',
                'Regius Ace' => 'RegiusAce',
                'RS3' => 'RS 3',
                'RS4' => 'RS 4',
                'RS5' => 'RS 5',
                'RS6' => 'RS 6',
                'RS7' => 'RS 7',
                'RX200t' => 'RX',
                'RX270' => 'RX',
                'RX300' => 'RX',
                'RX330' => 'RX',
                'RX350' => 'RX',
                'RX400h' => 'RX',
                'RX450h' => 'RX',
                'S-Class' => 'S-klasse',
                'S-type' => 'S-Type',
                'SC430' => 'SC',
                'SL-Class' => 'SL-klasse',
                'SLK-Class' => 'SLK-klasse',
                'Tiggo T11' => 'Tiggo (T11)',
                'UX200' => 'UX',
                'UX250h' => 'UX',
                'V-Class' => 'V-klasse',
            );
        foreach ($modelList as $oldModel => $newModel) {
            $carsDrom = (new carsDrom())->where('model', $oldModel)->get();
            foreach ($carsDrom as $carDrom) {
                $carDrom->model = $newModel;
                $carDrom->save();
            }
        }
    }

    public function autodrom() {
        set_time_limit(0);
//        $getMarks = (new carsDrom())->select('mark')->groupBy('mark')->get()->toArray();
//
//        $marks = [];
//        foreach ($getMarks as $mark) {
//            $marks[] = $mark['mark'];
//        }
//
//        $getModels = (new Cars())->select(['mark', 'model'])->whereIn('mark', $marks)->groupBy(['model', 'mark'])->orderBy('mark')->get()->toArray();
//
//        $models = [];
//        foreach ($getModels as $model) {
//            echo $model['model'] . '<br>';
//            $models[] = $model['model'];
//        }
//        die;
//
//        $getDromModels = (new carsDrom())->select(['model', 'mark'])->groupBy(['model', 'mark'])->get()->toArray();
//        $drommodels = [];
//        foreach ($getDromModels as $model) {
//            $drommodels[] = $model['model'];
//        }
//
//        $nonSearchModels = [];
//        foreach ($drommodels as $drommodel) {
//            $searchModel = array_search($drommodel, $models);
//            if (!$searchModel) {
//                $nonSearchModels[] = $drommodel;
//            }
//        }
//
//        foreach ($nonSearchModels as $model) {
//            echo $model . '<br>';
//        }
//
//        die;
//
//        echo implode('<br>', $models);
//        die;
//
//        $getModels = (new carsDrom())->select('model')->groupBy('model')->get()->toArray();
//        $models = [];
//        foreach ($getModels as $model) {
//            $models[] = $model['model'];
//        }
//        var_dump($models);
//
//        $getMarksDrom = (new Cars())->select('model')->whereIn('mark', $marks)->groupBy('model')->get();
//        var_dump($getMarksDrom->toArray());
//        die;
//        $cars = Cars::all();
//        $count = 0;
//        foreach ($cars as $car) {
//            if (!empty($car->engine)) {
//                $count++;
//            }
//        }
//
//        echo $count;
//        die;

        $allCount = 0;
        $findCount = 0;
        $newCount = 0;

        $carsDrom = (new carsDrom())->where('id', '<', 11923)->get();

        foreach ($carsDrom as $carDrom) {
            $allCount++;
            $boxType = $carDrom->box_type;
            if ($carDrom->box_type == 'МКПП') {
                $boxType = 'механика';
            } else if ($carDrom->box_type == 'АКПП') {
                $boxType = 'автомат';
            } else if ($carDrom->box_type == 'вариатор (CVT)') {
                $boxType = 'вариатор';
            }
            $whereData = [
              'engine_type' => $carDrom->engine_type,
              'volume' => $carDrom->volume,
              'box_type' => $boxType,
              'unit_type' => $carDrom->unit_type,
              'mark' => $carDrom->mark,
              'model' => $carDrom->model,
            ];
            $middleYear = ($carDrom->year_from + $carDrom->year_to)/2;
            $cars = Cars::where($whereData)->whereRaw("{$middleYear} BETWEEN `year_from` AND `year_to`")->get();
            if (count($cars) > 0) {
                foreach ($cars as $car) {
                    if (!empty($carDrom->engine)) {
                        $car->engine = $carDrom->engine;
                    }
                    if (!empty($carDrom->body_type)) {
                        preg_match('/[^<br \/>]+$/', $carDrom->body_type, $bodyMatches);
                        $car->body_type = $bodyMatches[0];
                    }
                    if (!empty($carDrom->body_code)) {
                        $car->body_code = $carDrom->body_code;
                    }
                    $car->market = $carDrom->market;
                    $car->url_drom = $carDrom->url;
                    $car->parse_type = 'merged';
                    $car->save();
                }

            } else {
                $car = new Cars();
                $findCount++;

                $car->mark = $carDrom->mark;
                $car->model = $carDrom->model;
                if (!empty($carDrom->body_type)) {
                    preg_match('/[^<br \/>]+$/', $carDrom->body_type, $bodyMatches);
                    $car->body_type = $bodyMatches[0];
                } else {
                    $car->body_type = '';
                }
                $car->body_code = $carDrom->body_code;
                $car->year_from = $carDrom->year_from;
                $car->year_to = $carDrom->year_to;
                $car->model_name = $carDrom->model_name;
                $car->volume = $carDrom->volume;
                $car->horse_power = $carDrom->horse_power;
                $car->engine_type = $carDrom->engine_type;
                $car->engine = $carDrom->engine;
                $car->box_type = $boxType;
                $car->unit_type = $carDrom->unit_type;
                $car->url = '';
                $car->url_drom = $carDrom->url;
                $car->market = $carDrom->market;
                $car->parse_type = 'drom';
                $newCount++;

                $car->save();
            }
            file_put_contents('dromnum.txt', $carDrom->id);
        }
        echo $allCount."<hr>".$newCount."<hr>".$findCount;
    }

    public function parseDrom() {
//        header('Content-Type: text/html; charset=windows-1251');
        $mainurl = 'https://www.drom.ru/catalog/';
        $mainContent = file_get_contents($mainurl);
        preg_match_all('|<a class="b-link" href="(.*?)">(.*?)<\/a>|is', $mainContent, $markMatches);
        foreach($markMatches[1] as $key => $url) {
            $mark = trim($markMatches[2][$key]);
            $content = iconv('windows-1251', 'utf-8', file_get_contents("https://www.drom.ru" . $url));
            preg_match_all('|<div class="b-selectCars__link">(.*?)</div>|is', $content, $links);

            foreach ($links[0] as $link) {
                preg_match('|href="(.*?)"|is', $link, $matchlink);
                preg_match('/>(.*?)<\/a>/', $link, $matchmodel);
                $model = $matchmodel[1];
                $link = $matchlink[1];
                $carUrl = new CarsUrl();
                $carUrl->mark = iconv('windows-1251', 'utf-8', $mark);
                $carUrl->model = $model;
                $carUrl->url = $link;
                $carUrl->save();
            }
        }
        die;


//        $mark = "Toyota";
//        $content = file_get_contents("https://www.drom.ru/catalog/toyota/");
//        $carList = preg_match_all('|<div class="b-selectCars__link">(.*?)</div>|is', $content, $links);
//
//        foreach ($links[0] as $link) {
//            preg_match('|href="(.*?)"|is', $link, $matchlink);
//            preg_match('/>(.*?)<\/a>/', $link, $matchmodel);
//            $model = $matchmodel[1];
//            $link = $matchlink[1];
//            $carUrl = new CarsUrl();
//            $carUrl->mark = $mark;
//            $carUrl->model = $model;
//            $carUrl->url = $link;
//            $carUrl->save();
//        }
//        die;
        set_time_limit(0);
        $carUrls = CarsUrl::where('id', '>', 0)->get();
        foreach ($carUrls as $carUrl) {
            $url = 'https://www.drom.ru'. $carUrl->url;
            $mark = $carUrl->mark;
            $model = $carUrl->model;
            $content = str_replace("                            ", "", file_get_contents($url));
            $content = iconv('windows-1251', 'utf-8', str_replace("\n", "", $content));

            preg_match_all('|href="(.*?)"class="b-info-block b-info-block_link b-info-block_theme_spring b-info-block_width_168"(.*?)<\/a>|is', $content, $matches);
            unset($matches[0][0]);
            foreach ($matches[0] as $match) {
                preg_match('/<span class="b-button__text b-link">(.*?)\.(.*?) - (.*?)\.(.*?)<\/span>/', $match, $carmain);
                preg_match('/href="(.*?)"class="b-info-block b-info-block_lin/', $match, $urlmatch);

                $yearfrom = $carmain[2];
                $yearto = $carmain[4];
                preg_match('/<br \/>(.*?)<\/div><\/div>/', $match, $bodytypematches);
                $body_type = $bodytypematches[1];
                $url = 'https://www.drom.ru'. $urlmatch[1];
                $content = iconv('windows-1251', 'utf-8', file_get_contents($url));
                preg_match('|<table class="b-table b-table_text-left b-table_text-size-s">(.*?)<\/table>|is', $content, $trmatch);

                if (count($trmatch) == 0) {
                    continue;
                }
                preg_match_all('|<tr class="(.*?)">(.*?)</tr>|is', $trmatch[0], $complmatches);

                foreach ($complmatches[0] as $key => $compl) {
                    if ($key == 0 || $complmatches[1][$key] == 'b-table__row b-table_align_center b-table__row_border_bottom b-table__row_padding_size-s') {
                        continue;
                    }
                    preg_match('/Рынок сбыта: (.*?)[\.\,]/', $content, $marketmatch);
                    $market = count($marketmatch) > 0 ? $marketmatch[1] : '';
                    preg_match('/<th colspan="[1-9]">(.*?)<\/th>/', $compl, $carinfo);
                    preg_match('/(.*?) л, (.*?) л.с., (.*?), (.*?), (.*?) привод/', $carinfo[1], $complmatch);
                    if (count($complmatch) == 0) {
                        continue;
                    }
                    $volume = $complmatch[1];
                    $horse = $complmatch[2];
                    $engine_type = $complmatch[3];
                    $box_type = $complmatch[4];
                    $unit_type = $complmatch[5];

                    preg_match_all('/<td>(.*?)<\/td>/', $complmatches[0][$key+1], $bodymatch);
                    preg_match('/class="b-link">(.*?)<\/a>/i', $complmatches[0][$key+1], $enginematch);
                    if (count($enginematch) == 0) {
                        continue;
                    }
                    if (count($bodymatch[1]) == 0) {
                        continue;
                    }
                    $engine = $enginematch[1];
                    $car = new carsDrom();
                    $car->mark = $mark;
                    $car->model = $model;
                    $car->body_type = $body_type;
                    $car->body_code = $bodymatch[1][count($bodymatch[1]) - 1];
                    $car->year_from = (int)$yearfrom;
                    $car->year_to = (int)$yearto;
                    $car->model_name = '';
                    $car->volume = $volume;
                    $car->horse_power = $horse;
                    $car->engine_type = $engine_type;
                    $car->box_type = $box_type;
                    $car->unit_type = $unit_type;
                    $car->engine = $engine;
                    $car->market = $market;
                    $car->url = $url;
                    $car->save();
                }
            }
        }
        die;

        $url = 'https://www.drom.ru/catalog/toyota/altezza/';

        var_dump($matches);

        die;
    }

    public static function getCarsPage ($url) {
        $cookie = 'suid=47a7cd8212a33e2006ff3a63999d4db0.e8652bf560aacea1d60b6a5468e7cca5; gdpr=1; cto_lwid=8a6f3a5e-dc17-4240-a89b-a9dd31cd8156; _ym_uid=1540581137949159490; _ym_d=1540581137; _csrf_token=4b786a3cfda497c11f9c949f4caac788751f827b649fbb81; autoruuid=g5c4c55b02sgefno3it48uh8k7c4p2ts.b78790a1e9958e8fc6c9635b5f7cb74e; yandexuid=1818890331537527685; my=YwA%3D; los=1; af_lpdid=14:768495248; rheftjdd=rheftjddVal; spravka=dD0xNTQ4NTEwODE3O2k9MTc4LjEyMi4yMDYuMjEwO3U9MTU0ODUxMDgxNzE2OTA3NzQ3MztoPWI3NWE0ZGY2ZWJmMWNhOTZhYjQ1ZmYyOGNjODVlZTdm; los=1; autoru_sid=a%3Ag5c4c55b02sgefno3it48uh8k7c4p2ts.b78790a1e9958e8fc6c9635b5f7cb74e%7C1549111344256.604800.g5lgILyusB0E9SOpZMnu3w.1wptL57QzURYmu-FGhgl67Zyvw312YGKdVJGInsQE-U; mmm-search-accordion-is-open-cars=%5B0%5D; counter_ga_all7=1; _ga=GA1.2.1748627266.1548955517; _gid=GA1.2.468788690.1549113913; crookie=l9H+J2HmTqOJq99aTaYKjg9RQxBmIDPRFuQ1g+5Ly7W0r4ZqnTLjL79ZiOljHK5oXXU+N1uAkEo38s6yjYf22rqYWxQ=; crookie=l9H+J2HmTqOJq99aTaYKjg9RQxBmIDPRFuQ1g+5Ly7W0r4ZqnTLjL79ZiOljHK5oXXU+N1uAkEo38s6yjYf22rqYWxQ=; cmtchd=MTU0OTExMzkxNTIwMw==; cmtchd=MTU0OTExMzkxNTIwMw==; _ym_isad=1; _ym_wasSynced=%7B%22time%22%3A1549201321142%2C%22params%22%3A%7B%22eu%22%3A0%7D%2C%22bkParams%22%3A%7B%7D%7D; from=direct; X-Vertis-DC=myt; _gat=1; from_lifetime=1549207946816';
        $curl=curl_init();
        curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
        curl_setopt($curl, CURLOPT_ENCODING, '');
        curl_setopt($curl, CURLOPT_URL, $url);
//        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_COOKIE, $cookie);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        $out = curl_exec($curl);
        return $out;
    }

    public function addBoxValues () {
        set_time_limit(0);
        $carUrls = (new Cars())->select('url')->groupBy('url')->get();
        foreach ($carUrls as $carUrl) {
            if (!empty($carUrl->url)) {
                $out = self::getCarsPage($carUrl->url. 'specifications/');
                preg_match_all('|<tr class="catalog-table__row(.*?)">(.*?)<\/tr>|is', $out, $complmatches);
                $engineType = '';
                foreach ($complmatches[0] as $key => $compl) {
                    if (empty($complmatches[1][$key])) {
                        preg_match('|<div class="catalog-table__group-title">(.*?)<\/div>|is', $compl, $etype);
                        $engineType = isset($etype[1]) ? $etype[1] : $engineType;
                        continue;
                    }
                    preg_match('|link__control i-bem" role="link" href="(.*?)"|is', $compl, $urlMatches);
                    preg_match('|catalog-table__cell_alias_gear">(.*?)<\/td>|is', $compl, $boxTypeMatch);
                    preg_match('|catalog-table__cell_alias_power">(.*?) л.c.<\/td>|is', $compl, $powerMatch);
                    if (!isset($boxTypeMatch[1]) || !isset($powerMatch[1])) {
                        continue;
                    }
                    $boxType = $boxTypeMatch[1];
                    $power = $powerMatch[1];
                    $whereParams = [
                        'engine_type' => $engineType,
                        'box_type'    => $boxType,
                        'horse_power' => $power,
                    ];
                    $findedCars = (new Cars())->where($whereParams)->get();
                    $out = self::getCarsPage($urlMatches[1]);
                    preg_match('|<dt class="list-values__label">Количество передач<\/dt><dd class="list-values__value">(.*?)<\/dd>|is', $out, $boxMatches);
//                    var_dump($boxMatches);
                    if (isset($boxMatches[1])) {
                        foreach ($findedCars as $findedCar) {
                            $findedCar->box_val = $boxMatches[1];
                            $findedCar->save();
                        }
                    }
                }
            }
        }
    }

    public function addBoxValues1 () {
        $host = 'http://localhost:4444/wd/hub';
        $driver = RemoteWebDriver::create($host, DesiredCapabilities::chrome());
        $driver->get('https://auto.ru/catalog/cars/');
        $driver->findElement(WebDriverBy::id('confirm-button'))->click();
        $carUrls = (new Cars())->select('url')->groupBy('url')->get();
        foreach ($carUrls as $carUrl) {
            if (!empty($carUrl->url)) {
                $driver->get($carUrl->url . 'specifications/');
                $driver->wait()->until(
                    WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::className('catalog-table__inner'))
                );
                $content = $driver->getPageSource();
                preg_match_all('|<tr class="catalog-table__row(.*?)">(.*?)<\/tr>|is', $content, $complmatches);
                $engineType = '';
                sleep(2);
                foreach ($complmatches[0] as $key => $compl) {
                    if (empty($complmatches[1][$key])) {
                        preg_match('|<div class="catalog-table__group-title">(.*?)<\/div>|is', $compl, $etype);
                        $engineType = $etype[1];
                        continue;
                    }
                    $driver->findElement(WebDriverBy::xpath("//table//tr[{$key}]"))->click();
                }
//                var_dump($complmatches);
                die;
            }
        }
        die;
    }

    public function toElastic () {
        set_time_limit(0);
        $client = ClientBuilder::create()->build();
        $params = [
            'index' => 'autofilters',
            'type'  => 'cars',
        ];
        try {
            $client->indices()->delete(['index' => 'autofilters']);
        } catch (\Exception $e) {

        }

        $indexsettings = '{
    "settings": {
            "analysis": {
      "filter": {
        "english_stemmer": {
          "type": "stemmer",
          "language": "english" 
        },
        "russian_stemmer": {
          "type": "stemmer",
          "language": "russian" 
        }
      },
      "analyzer": {
        "car_analyzer": {
          "tokenizer": "standard",
          "filter": [
            "lowercase",
            "english_stemmer",
            "russian_stemmer"
          ]
        }
      }
    }
    }, 
    "mappings": {
        "cars": {
    "properties": {
            "mark": {
                "type": "text",
                "analyzer": "car_analyzer",
                "search_analyzer": "car_analyzer"
               },
               "model": {
                "type": "text",
                "analyzer": "car_analyzer",
                "search_analyzer": "car_analyzer"
               },
                "body_type": {
                "type": "text",
                "analyzer": "car_analyzer",
                "search_analyzer": "car_analyzer"
               },
                "body_code": {
                  "type": "text",
                "analyzer": "car_analyzer",
                "search_analyzer": "car_analyzer"
               },
                "model_name": {
                  "type": "text",
                "analyzer": "car_analyzer",
                "search_analyzer": "car_analyzer"
               },
                "volume": {
                  "type": "text",
                "analyzer": "car_analyzer",
                "search_analyzer": "car_analyzer"
               },
                "horse_power": {
                  "type": "text",
                "analyzer": "car_analyzer",
                "search_analyzer": "car_analyzer"
               },
                "engine_type": {
                  "type": "text",
                "analyzer": "car_analyzer",
                "search_analyzer": "car_analyzer"
               },
                "box_type": {
                  "type": "text",
                "analyzer": "car_analyzer",
                "search_analyzer": "car_analyzer"
               },
                "unit_type": {
                  "type": "text",
                "analyzer": "car_analyzer",
                "search_analyzer": "car_analyzer"
               }
    }
}
}
}';
        $settings = [];
        $settings['body'] = (array)json_decode($indexsettings);
        $settings['index'] = 'autofilters';

        $client->indices()->create($settings);

        $cars = Cars::where('id', '>', 0)->get();
        foreach ($cars as $car) {
            $params['id'] = $car->id;
            $body = $car->toArray();
            unset($body['id']);
            unset($body['created_at']);
            unset($body['updated_at']);
            unset($body['url']);
            $paramsToSearch = [
                $body['mark'],
                $body['model'],
                $body['body_type'],
                $body['body_code'],
                $body['model_name'],
                $body['volume'],
                $body['horse_power'],
                $body['engine_type'],
                $body['box_type'],
                $body['unit_type'],
            ];
            $body['search_query'] = implode(' ', $paramsToSearch);
           $params['body'] = $body;
           $client->index($params);
        }

    }

    public function merge () {
        $carGenerations = carGenerations::all();
        foreach ($carGenerations as $carGeneration) {
            $carComplectations = CarComplectations::where('generation_id', $carGeneration->id)->get();
            foreach ($carComplectations as $carComplectation) {
                $car = new Cars();
                $car->mark = $carGeneration->mark;
                $car->model = $carGeneration->model;
                $car->body_type = $carGeneration->body_type;
                $car->body_code = $carGeneration->body_code;
                $car->year_from = $carGeneration->year_from;
                $car->year_to = $carGeneration->year_to == 'н.в.' ? 2019 : $carGeneration->year_to;
                $car->model_name = $carComplectation->model_name;
                $car->volume = preg_replace('/[^0-9.]/','',$carComplectation->volume);
                $car->horse_power = $carComplectation->horse_power;
                $car->engine_type = $carComplectation->engine_type;
                $car->box_type = $carComplectation->box_type;
                $car->unit_type = $carComplectation->unit_type;
                $car->url = $carGeneration->url;
                $car->save();
            }
        }
    }

    public function parseAutoRu()
    {
        set_time_limit(0);
        $carGenerations = carGenerations::all();
        foreach ($carGenerations as $carGeneration) {
            $out = self::getCarsPage($carGeneration->url . 'specifications/');
            preg_match_all('|<tr class="catalog-table__row(.*?)">(.*?)<\/tr>|is', $out, $complmatches);
            foreach ($complmatches[0] as $compl) {
                preg_match('|link__control i-bem" role="link" href="(.*?)"|is', $compl, $urlMatch);
                if (count($urlMatch) < 1) {
                    continue;
                }
                preg_match('|"key":"specifications"}}\'>(.*?)<\/a>|is', $compl, $modelmatch);
                $model = $modelmatch[1];
                $complout = self::getCarsPage($urlMatch[1]);
                preg_match('|<dd class="list-values__value">(.*?) л<\/dd>|is', $complout, $volumematch);
                $volume = $volumematch[1];
                preg_match('|Мощность<\/dt><dd class="list-values__value">(.*?) л.с.<\/dd>|is', $complout, $powermatch);
                $power = $powermatch[1];
                preg_match('|Коробка передач<\/dt><dd class="list-values__value">(.*?)<\/dd>|is', $complout, $boxmatch);
                $box = $boxmatch[1];
                preg_match('|Тип двигателя<\/dt><dd class="list-values__value">(.*?)<\/dd>|is', $complout, $enginematch);
                $engineType = $enginematch[1];
                preg_match('|Привод</dt><dd class="list-values__value">(.*?)</dd>|is', $complout, $unitmatch);
                $unitType = $unitmatch[1];
                preg_match('|Количество передач</dt><dd class="list-values__value">(.*?)</dd>|is', $complout, $boxvalmatch);
                $boxVal = isset($boxvalmatch[1]) ? $boxvalmatch[1] : 0;
                $car = new Cars();
                $car->mark = $carGeneration->mark;
                $car->model = $carGeneration->model;
                $car->body_type = $carGeneration->body_type;
                $car->body_code = $carGeneration->body_code;
                $car->year_from = $carGeneration->year_from;
                $car->year_to = $carGeneration->year_to == 'н.в.' ? 2019 : $carGeneration->year_to;
                $car->model_name = $model;
                $car->volume = $volume;
                $car->horse_power = $power;
                $car->engine_type = $engineType;
                $car->box_type = $box;
                $car->unit_type = $unitType;
                $car->box_val = $boxVal;
                $car->url = $urlMatch[1];
                $car->save();
            }
        }
    }

    public function parseComplectations () {
        set_time_limit(0);
        $host = 'http://localhost:4444/wd/hub';
        $driver = RemoteWebDriver::create($host, DesiredCapabilities::chrome());
        $driver->get('https://auto.ru/catalog/cars/');
        $driver->findElement(WebDriverBy::id('confirm-button'))->click();
        $generations = carGenerations::all()->where('id', '>', '8843');
        foreach ($generations as $generation) {
            $driver->get($generation->url);
            if (count($driver->findElements(WebDriverBy::className('reload-button'))) !== 0) {
                $driver->get($generation->url);
            }
            $driver->wait()->until(
                WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::className('catalog-table'))
            );


            $content = $driver->getPageSource();
            preg_match_all('|<tr class="catalog-table__row catalog-table__row_highlight(.*?)">(.*?)<\/tr>|is', $content, $complectations);

            foreach ($complectations[0] as $ckey => $complectation) {
                preg_match('|<td class="catalog-table__cell catalog-table__cell_alias_name">(.*?)</td>|is', $complectation, $compln);
                preg_match('|<td class="catalog-table__cell catalog-table__cell_alias_engine">(.*?)</td>|is', $complectation, $complEng);
                if (!isset($complEng[1])){
                    $find = false;
                    $startkey = $ckey;
                    while (!$find) {
                        $startkey--;
                        preg_match('|<td class="catalog-table__cell catalog-table__cell_alias_name">(.*?)</td>|is', $complectations[0][$startkey], $compln);
                        preg_match('|<td class="catalog-table__cell catalog-table__cell_alias_engine">(.*?)</td>|is', $complectations[0][$startkey], $complEng);
                        if (isset($complEng[1])) {
                            $modelName = isset($compln[1]) ? $compln[1] : '';
                            $explEng = explode(' ', $complEng[1]);
                            $volume = $explEng[0];
                            $horse = $explEng[1];
                            $engineType = $explEng[3];
                            $find = true;
                        }
                    }
                } else {

                    $modelName = isset($compln[1]) ? $compln[1] : '';
                    $explEng = explode(' ', $complEng[1]);
                    $volume = $explEng[0];
                    $horse = $explEng[1];
                    $engineType = $explEng[3];
                }
                preg_match('|<td class="catalog-table__cell catalog-table__cell_alias_gearbox"><a (.*?)>(.*?)</a></td>|is', $complectation, $box);
                $boxType = $box[2];
                preg_match('|<td class="catalog-table__cell catalog-table__cell_alias_drive">(.*?)</td>|is', $complectation, $unit);
                $unitType = $unit[1];

                $c = new CarComplectations();
                $c->generation_id = $generation->id;
                $c->model_name = $modelName;
                $c->volume = $volume;
                $c->horse_power = $horse;
                $c->engine_type = $engineType;
                $c->box_type = $boxType;
                $c->unit_type = $unitType;
                $c->save();
            }
        }

    }

    public static function parseModels ($id = 0) {
        set_time_limit(0);
        $host = 'http://localhost:4444/wd/hub';
        $driver = RemoteWebDriver::create($host, DesiredCapabilities::chrome());
        $driver->get('https://auto.ru/catalog/cars/');
        $driver->findElement(WebDriverBy::id('confirm-button'))->click();
        $carUrls = CarsUrl::all();
        foreach ($carUrls as $carUrl) {
            if ($id > $carUrl->id) {
                continue;
            }
            try {
                $driver->get($carUrl->url);
                $driver->wait()->until(
                    WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::className('catalog-all-text-list'))
                );


                $content = $driver->getPageSource();
                preg_match_all('|<dt class="catalog-all-text-list__title">(.*?)</dt>|is', $content, $generations);
                preg_match_all('|<dd class="catalog-all-text-list__desc">(.*?)</dd>|is', $content, $types);

                preg_match_all('|<div class="search-form-v2-mmm__breadcrumbs search-accordion__header">(.*?)</div>|is', $content, $breadcrumb);
                preg_match_all('|<a (.*?)>(.*?)</a>|is', $breadcrumb[0][0], $markpm);

                $mark = $markpm[2][0];
                $markseries = $markpm[2][1];
                foreach ($generations[0] as $key => $generation) {
                    preg_match('|<dt class="catalog-all-text-list__title">(.*?)<div|is', $generation, $years);
                    preg_match('|<div class="catalog-all-text-list__subtext">(.*?)</div>|is', $generation, $bodies);
                    $bodeType = $bodies[1];

                    $explYears = explode(" – ", $years[1]);

                    $yearFrom = $explYears[0];
                    $yearTo = $explYears[1];
                    preg_match_all('|<div class="mosaic (.*?)">(.*?)</div>|is', $types[1][$key], $typesForGeneration);

                    foreach ($typesForGeneration[0] as $type) {
                        preg_match('|href="(.*?)" (.*?)><div class="mosaic__image"><div class="mosaic__image-inner" title="(.*?)"|is',$type, $car);
                        $body = $car[3];
                        $url = $car[1];
                        $carGeneration = new carGenerations();
                        $carGeneration->mark = $mark;
                        $carGeneration->model = $markseries;
                        $carGeneration->body_code = $bodeType;
                        $carGeneration->body_type = $body;
                        $carGeneration->year_from = $yearFrom;
                        $carGeneration->year_to = $yearTo;
                        $carGeneration->url = $url;
                        $carGeneration->save();
                    }
                }
            } catch (Exception $e) {
                self::parseModels($carUrl->id);
            }

        }
    }

    public function carModels() {
        set_time_limit(0);
        self::parseModels();
    }

    public function carsList () {
        die;
        $driver->wait()->until(
            WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::className('catalog-all-text-list__title'))
        );
        $host = 'http://localhost:4444/wd/hub';
        $driver = RemoteWebDriver::create($host, DesiredCapabilities::chrome());

        for ($i = 1; $i <= 26; $i++) {
            $driver->get('https://auto.ru/catalog/cars/all/?page_num=' .$i. '&view_type=list');
            if ($i == 1) {
                $driver->findElement(WebDriverBy::id('confirm-button'))->click();
            }
            $source = $driver->getPageSource();

            $pattern = '|<li class="mmm__item"><a class="link link_theme_auto link__control i-bem" role="link" href="(.*?)" data-bem|is';
            preg_match_all($pattern, $source, $matches);

            foreach ($matches[1] as $url) {
                $listModel = new CarsUrl();
                $listModel->url = $url;
                $listModel->save();
            }
        }
        die;
        $client = new Client(['cookies' => true]);
        $cook = 'suid=47a7cd8212a33e2006ff3a63999d4db0.e8652bf560aacea1d60b6a5468e7cca5; gdpr=1; cto_lwid=8a6f3a5e-dc17-4240-a89b-a9dd31cd8156; _ym_uid=1540581137949159490; _ym_d=1540581137; _csrf_token=4b786a3cfda497c11f9c949f4caac788751f827b649fbb81; autoru_sid=a%3Ag5c4c55b02sgefno3it48uh8k7c4p2ts.b78790a1e9958e8fc6c9635b5f7cb74e%7C1548506544256.604800.NFWv4k_Li2_9ae_zjDXiKw.Z8mQDxIE6M9OaB0hNDV0kCUv4wMDbKyxSmgBslcrJQc; autoruuid=g5c4c55b02sgefno3it48uh8k7c4p2ts.b78790a1e9958e8fc6c9635b5f7cb74e; X-Vertis-DC=myt; yandexuid=1818890331537527685; my=YwA%3D; los=1; _ym_wasSynced=%7B%22time%22%3A1548506548439%2C%22params%22%3A%7B%22eu%22%3A0%7D%2C%22bkParams%22%3A%7B%7D%7D; from=other; af_lpdid=14:768495248; rheftjdd=rheftjddVal; crookie=4wkGJrCjqlihRTE/uphV+0PZSFHS8LHGsCHKY2EpI99BHENFgXw51I1ly8K571hxCDAFWO3BTHVWJvEsZx23zSAhRVs=; cmtchd=MTU0ODUwNjU2MTQ2MQ==; _ym_isad=1; mmm-search-accordion-is-open-cars=%5B0%5D; spravka=dD0xNTQ4NTEwODE3O2k9MTc4LjEyMi4yMDYuMjEwO3U9MTU0ODUxMDgxNzE2OTA3NzQ3MztoPWI3NWE0ZGY2ZWJmMWNhOTZhYjQ1ZmYyOGNjODVlZTdm; from_lifetime=1548514596642';
        $cookies = [];
        $explcook = explode(';', $cook);
        foreach ($explcook as $ecook) {
            $expl = explode("=", $ecook);

            $cookies[$expl[0]] = $expl[1];
        }

        $cookie = CookieJar::fromArray([
            $cookies
        ], 'auto.ru');


        $response = $client->request('GET', 'https://auto.ru/checkcaptcha', ['query' => [
            'key' => '002NU1v08DOXGITghwGrW5kCsg56EDWj_0/1548516111/46a2407a0e1adcbc0913a8f3e43c0228_f35a4709ea19b674690f599af3da1ba7',
            'retpath' => 'https://auto.ru/catalog/cars/all?page_num=1&view_type=list_4e65bea005bb8f49b4d84e2965a926a1',
            'rep' => 'trallisgarage'
        ]]);


        echo $response->getBody();
        die;
        $options = [
            'cookies' => $cookie,
//            'proxy'   => ['https' => '3XNKU7:nMz8LQ@217.29.53.203:28909'],
        ];
        $response = $client->request('GET', 'https://auto.ru/catalog/cars/all/?page_num=1&view_type=list', $options);
        $page = $response->getBody();
//        var_dump($response->getHeaders());

//        $page = file_get_contents('autoru.txt');
        echo $page;
//        $pattern = '|<a class="link link_theme_auto link__control i-bem link_js_inited" role="link" href="(.*?)" (.*?)>(.*?)</a>|is';
        $pattern = '|<li class="mmm__item"><a class="link link_theme_auto link__control i-bem" role="link" href="(.*?)" data-bem|is';
        preg_match_all($pattern, $page, $matches);
        foreach ($matches[1] as $url) {
            $listModel = new CarsUrl();
            $listModel->url = $url;
            $listModel->save();
        }
    }
}
