<?php

namespace App\Http\Controllers;

use App\CarComplectations;
use App\carGenerations;
use App\Cars;
use App\Services\SmsService;
use Elasticsearch\ClientBuilder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    public function test () {
        (new SmsService())->getDevices();

    }

    public static function getTable () {
        if (!Auth::check()) {
            return 'cars_ferio';
        } else {
            return (bool)Auth::user()->search_base ? 'cars_ferio' : 'cars';
        }
    }

    public function editCar (Request $data) {
        $carParams = $data->carParams;
        $car = (new Cars())->where('id', $carParams['id'])->first();
        if ($car) {
            $car->mark = $carParams['mark'];
            $car->model = $carParams['model'];
            $car->body_code = $carParams['body_code'];
            $car->model_name = $carParams['model_name'] ?: '';
            $car->body_type = $carParams['body_type'];
            $car->year_from = $carParams['year_from'];
            $car->year_to = $carParams['year_to'];
            $car->engine_type = $carParams['engine_type'];
            $car->engine = $carParams['engine'] ?: '';
            $car->volume = $carParams['volume'];
            $car->horse_power = $carParams['horse_power'];
            $car->box_type = $carParams['box_type'];
            $car->market = $carParams['market'] ?: '';
            $car->save();
        }
        return $data->toArray();
    }

    public function getInputResults (Request $data) {
        $searchText = $data['text'];
        $client = ClientBuilder::create('localhost:9200')->build();

        $params = [
            'index' => 'autofilters',
            'type'  => 'cars',
            'body'  => [
                'size' => 100,
                'sort' => ['_score'],
                'query' => [
                    'multi_match' => [
                        'query' => $searchText,
                        'fields' => [
                            'mark^4',
                            'model^1.5',
                            'body_type',
                            'body_code',
                            'model_name',
                            'volume',
                            'horse_power',
                            'engine_type',
                            'box_type',
                            'unit_type',
                        ],
                        'type' => 'most_fields'
                    ]
                ]
            ]
        ];

        $response = $client->search($params);
        $results = [];
        foreach ($response['hits']['hits'] as $res) {
            $results[] = $res['_source'];
        }
        return $results;
    }

    public function getModels (Request $data) {
        $getModels = carGenerations::select('model')->groupBy('model')->where('mark', $data['mark'])->get();
        $models = [];
        foreach ($getModels as $model) {
            $models[] = ['id' => $model->model, 'label' => $model->model];
        }
        return $models;
    }

    public static function addSelectParams($selectParams, $params) {
        foreach ($params as $key => $value) {
            if (empty($value)) {
                continue;
            }
            switch ($key) {
                case 'mark':
                    $selectParams->where('mark', '=', $value);
                    break;
                case 'model':
                    $selectParams->whereIn('model', $value);
                    break;
                case 'body':
                    $selectParams->whereIn('body_type', $value);
                    break;
                case 'box':
                    $selectParams->whereIn('box_type', $value);
                    break;
                case 'volume':
                    $selectParams->whereIn('volume', $value);
                    break;
                case 'engine':
                    $selectParams->whereIn('engine_type', $value);
                    break;
                case 'power':
                    $selectParams->where('horse_power', $value);
                    break;
                case 'year':
                    $select = '';
                    foreach ($value as $year) {
                        if (!empty($select)) {
                            $select.= ' OR ';
                        }
                        $select.= "({$year} BETWEEN `year_from` AND `year_to`)";
                    }
                    $selectParams->whereRaw('(' . $select .')');
                    break;

            }
        }
        return $selectParams;
    }

    public function getResults (Request $data) {
        $params = $data->toArray();
        $selectParams = DB::table(self::getTable());
        self::addSelectParams($selectParams, $params);
        if (isset($params['filter'])) {
            unset($params[$params['filter']]);
        }
        $params = self::getParams($params);
        $results = $selectParams->limit(150)->get()->toArray();
        foreach ($results as $key => $result) {
            $results[$key]->volume  = number_format($result->volume, 1);
        }
        return ['results' => $results, 'params' => $params];
    }

    public static function prepareSelect ($whereParams = false) {
        $select = DB::table(self::getTable());
        if ($whereParams) {
            self::addSelectParams($select, $whereParams);
        }
        return $select;
    }

    public static function getParams ($whereParams = false) {
        $getMarks = self::prepareSelect($whereParams)->select('mark')->groupBy('mark')->get();
        $marks = [];
        foreach ($getMarks as $mark) {
            $marks[] = $mark->mark;
        }

        $models = [];
        if ($whereParams) {
            $getModels = self::prepareSelect($whereParams)->select('model')->groupBy('model')->get();
            foreach ($getModels as $model) {
                $models[] = $model->model;
            }
        }

        $getBodies = self::prepareSelect($whereParams)->select('body_type')->groupBy('body_type')->get();
        $bodies = [];
        foreach ($getBodies as $body) {
            $bodies[] = $body->body_type;
        }

        $getBoxes = self::prepareSelect($whereParams)->select('box_type')->groupBy('box_type')->get();
        $boxes = [];
        foreach ($getBoxes as $box) {
            $boxes[] = $box->box_type;
        }

        $getVolumes = self::prepareSelect($whereParams)->select('volume')->where('volume', '>', 0)->groupBy('volume')->get();
        $volumes = [];
        foreach ($getVolumes as $volume) {
            $volumes[] = number_format($volume->volume, 1);
        }

        $getEngines = self::prepareSelect($whereParams)->select('engine_type')->groupBy('engine_type')->get();
        $engines = [];
        foreach ($getEngines as $engine) {
            $engines[] = $engine->engine_type;
        }

        $getPowers = self::prepareSelect($whereParams)->select('horse_power')->groupBy('horse_power')->get();
        $powers = [];
        foreach ($getPowers as $power) {
            $powers[] = $power->horse_power;
        }

        $getYears = self::prepareSelect($whereParams)->selectRaw('min(year_from) as year_min, max(year_to) as year_max')->first();
        $years = [];
        for ($y = $getYears->year_min; $y <= $getYears->year_max; $y++) {
            $years[] = $y;
        }

        $params = [
            'marks'   => $marks,
            'models'   => $models,
            'bodies'  => $bodies,
            'boxes'   => $boxes,
            'volumes' => $volumes,
            'engines' => $engines,
            'powers'  => $powers,
            'years'   => array_reverse($years),
        ];
        return $params;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function admin()
    {
        $params = self::getParams();
        return view('admin', [
            'params'      => $params,
            'is_admin'    => Auth::user()->is_admin,
            'search_base' => Auth::user()->search_base,
        ]);
    }

    public function changeBase (Request $data) {
        Auth::user()->search_base = $data['searchBase'];
        Auth::user()->save();
//        var_dump($data->toArray());
    }

    public function client(Request $data)
    {
        $selectParams = isset($data['params']) ? json_decode($data['params']) : [];
        if (isset($selectParams->year)) {
            $selectParams->year = [$selectParams->year];
        }
        if (isset($selectParams->box)) {
            $selectParams->box = [mb_strtolower($selectParams->box)];
        }
        if (isset($selectParams->model)) {
            $selectParams->model = [$selectParams->model];
        }
        if (isset($selectParams->mark)) {
            $whereparams = ['mark' => $selectParams->mark];
        } else {
            $whereparams = [];
        }
        $params = self::getParams($whereparams);
        return view('client',
            [
                'params' => $params,
                'selectParams' => $selectParams
            ]
        );
    }
}
