<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    public function editApiSettings (Request $data) {
        if (!empty($data['crm_email'])) {
            Auth::user()->crm_email = $data['crm_email'];
            Auth::user()->crm_key = $data['api_key'];
            Auth::user()->save();
            return redirect(route('editApiSettings'));
        }
        return view('api_settings');
    }
}
