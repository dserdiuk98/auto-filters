<?php

namespace App\Services;

use Illuminate\Support\Facades\DB;

class ParseFerioService {

    public static function formatMarksAndModels () {
        $rows = DB::table('ferio_models')->join('ferio_brands', 'ferio_models.brand_id', '=', 'ferio_brands.brand_id')
                                         ->select('ferio_models.*', 'ferio_brands.brand_name')->get();
//        die;
        foreach ($rows as $row) {
            DB::table('cars_ferio')->where([
                ['mark', '=', (string)$row->brand_id],
                ['model', '=', (string)$row->model_id]
            ])->update(['mark' => $row->brand_name, 'model' => $row->model_name]);

        }
    }

    public static function formatTable () {
        $allResults = false;
        $limitFrom = 0;
        while (!$allResults) {
            $car = DB::table('cars_ferio')->orderBy('year_from', 'asc')->offset($limitFrom)->limit(1)->first();
            if (!$car) {
                $allResults = true;
            } else {
                $cars = DB::table('cars_ferio')->where([
                    ['mark' ,'=', $car->mark],
                    ['model' ,'=', $car->model],
                    ['body_type' ,'=', $car->body_type],
                    ['body_code','=', $car->body_code],
                    ['model_name' ,'=', $car->model_name],
                    ['volume'    ,'=', $car->volume],
                    ['horse_power' ,'=', $car->horse_power],
                    ['engine_type' ,'=', $car->engine_type],
                    ['engine'     ,'=', $car->engine],
                    ['box_type'   ,'=', $car->box_type],
                    ['id', '<>', $car->id]
                ])->orderBy('year_from', 'asc')->get();
                if (count($cars) > 0) {
                    $yearTo = $cars[count($cars) - 1]->year_to;
                    $ids = [];
                    foreach ($cars as $c) {
                        $ids[] = $c->id;
                    }
                    DB::table('cars_ferio')->where('id', $car->id)->update(['year_to' => $yearTo]);
                    DB::table('cars_ferio')->whereIn('id', $ids)->delete();
                }
            }
            $limitFrom++;
        }
    }

    public static function request($action, $params) {
        $params['action'] = $action;

        $curl=curl_init();
        curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
        curl_setopt($curl, CURLOPT_ENCODING, '');
        curl_setopt($curl, CURLOPT_URL, 'http://api.ferio.ru/Display/modules/extendedAPI.responce.php?'. http_build_query($params));
//        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($curl, CURLOPT_HEADER, false);

        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        $out = curl_exec($curl);
        return json_decode($out);
//        return json_decode(file_get_contents('http://api.ferio.ru/Display/modules/extendedAPI.responce.php?'. http_build_query($params)));
    }

    public static function parseAll () {
        $models = DB::table('ferio_models')->get();
        foreach ($models as $model) {
            for ($year = 1970; $year <= 2019; $year++) {
                $engineVols = self::request('getEngineVolByYearModelJSON', ['year' => $year, 'model_id' => $model->model_id]);
                if (empty($engineVols)) {
                    continue;
                }
                foreach ($engineVols as $engineVol) {
                    $engineTypes = self::request('getEngineTypeByYearModelEngineVolJSON', [
                        'year' => $year,
                        'model_id' => $model->model_id,
                        'engine_vol' => $engineVol
                    ]);
                    if (empty($engineTypes)) {
                        continue;
                    }
                    foreach ($engineTypes as $engineType) {
                        $enginePowers = self::request('getHorsepowerByYearModelEngineVolEngineTypeJSON', [
                            'year' => $year,
                            'model_id' => $model->model_id,
                            'engine_vol' => $engineVol,
                            'engine_type_id' => $engineType->engine_type_id
                        ]);
                        if (empty($enginePowers)) {
                            continue;
                        }
                        foreach ($enginePowers as $enginePower) {
                            $engineUnits = self::request('getDriveByYearModelEngineVolEngineTypeHorsepowerJSON', [
                                'year' => $year,
                                'model_id' => $model->model_id,
                                'engine_vol' => $engineVol,
                                'engine_type_id' => $engineType->engine_type_id,
                                'horsepower' => $enginePower,
                            ]);
                            if (empty($engineUnits)) {
                                continue;
                            }
                            foreach ($engineUnits as $engineUnit) {
                                $engineBodyTypes = self::request('getBodyByYearModelEngineVolEngineTypeHorsepowerDriveJSON', [
                                    'year' => $year,
                                    'model_id' => $model->model_id,
                                    'engine_vol' => $engineVol,
                                    'engine_type_id' => $engineType->engine_type_id,
                                    'horsepower' => $enginePower,
                                    'drive_id' => $engineUnit->drive_id
                                ]);
                                if (empty($engineBodyTypes)) {
                                    continue;
                                }
                                foreach ($engineBodyTypes as $engineBodyType) {
                                    $engineBoxes = self::request('getGearboxByYearModelEngineVolEngineTypeHorsepowerDriveBodyJSON', [
                                        'year' => $year,
                                        'model_id' => $model->model_id,
                                        'engine_vol' => $engineVol,
                                        'engine_type_id' => $engineType->engine_type_id,
                                        'horsepower' => $enginePower,
                                        'drive_id' => $engineUnit->drive_id,
                                        'body_id' => $engineBodyType->body_id,
                                    ]);
                                    if (empty($engineBoxes)) {
                                        continue;
                                    }
                                    foreach ($engineBoxes as $engineBox) {
                                        $bodies = self::request('getBodyCodeByYearModelEngineVolEngineTypeHorsepowerDriveBodyGearboxJSON', [
                                            'year' => $year,
                                            'model_id' => $model->model_id,
                                            'engine_vol' => $engineVol,
                                            'engine_type_id' => $engineType->engine_type_id,
                                            'horsepower' => $enginePower,
                                            'drive_id' => $engineUnit->drive_id,
                                            'body_id' => $engineBodyType->body_id,
                                            'gearbox_id' => $engineBox->gearbox_id,
                                        ]);
                                        if (empty($bodies)) {
                                            continue;
                                        }
                                        foreach ($bodies as $body) {
                                            $engines = self::request('getEngineCodeByYearModelEngineVolEngineTypeHorsepowerDriveBodyGearboxBodyCodeJSON', [
                                                'year' => $year,
                                                'model_id' => $model->model_id,
                                                'engine_vol' => $engineVol,
                                                'engine_type_id' => $engineType->engine_type_id,
                                                'horsepower' => $enginePower,
                                                'drive_id' => $engineUnit->drive_id,
                                                'body_id' => $engineBodyType->body_id,
                                                'gearbox_id' => $engineBox->gearbox_id,
                                                'body_code' => $body,
                                            ]);
                                            $car = [
                                                'mark' => $model->brand_id,
                                                'model' => $model->model_id,
                                                'body_type' => $engineBodyType->name,
                                                'body_code' => $body,
                                                'year_from' => $year,
                                                'year_to'   => $year,
                                                'model_name' => '',
                                                'volume'    => $engineVol,
                                                'horse_power' => $enginePower,
                                                'engine_type' => $engineType->name,
                                                'engine'     => count($engines) > 0 ? implode(';', $engines) : '',
                                                'box_type'   => $engineBox->name,
                                                'unit_type'  => $engineUnit->name
                                            ];
                                            DB::table('cars_ferio')->insert($car);
                                        }
                                    }
                                }
                            }
                        }

                    }
                }
            }
        }

    }

    public static function parseModels () {
        $brands = DB::table('ferio_brands')->get();
        $modelList = [];
        foreach ($brands as $brand) {
            for ($year = 1970; $year <= 2019; $year++) {
                $models = self::request('getModelByYearBrandJSON', ['year'=> $year, 'brand_id' => $brand->brand_id]);
                if (!$models) {
                    continue;
                }
                foreach ($models as $model) {
                    $modelList[$model->model_id] = (object)['brand_id' => $brand->brand_id, 'model_name' => $model->name];
                }
            }

        }

//        var_export($modelList);

        foreach ($modelList as $modelId => $model) {
            DB::table('ferio_models')->insert([
                'model_id' => $modelId, 'brand_id' => $model->brand_id, 'model_name' => $model->model_name
            ]);
        }
    }

    public static function parseBrands () {
        $brandList = [];
        for ($year = 1970; $year <= 2019; $year++) {
            $brands = self::request('getBrandByYearJSON', ['year'=> $year]);
            foreach ($brands as $brand) {
                $brandList[$brand->brand_id] = $brand->name;
            }
        }
        foreach ($brandList as $brandId => $brandName) {
            DB::table('ferio_brands')->insert([
                'brand_name' => $brandName, 'brand_id' => $brandId
            ]);
        }
    }
}
