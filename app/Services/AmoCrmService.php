<?php
namespace App\Services;

use App\User;
use Illuminate\Support\Facades\Auth;

class AmoCrmService {
    protected $userName;
    protected $apiKey;
    protected $subDomain;

    public function __construct($crmEmail = null, $crmKey = null)
    {
        $this->userName = $crmEmail ? $crmEmail : Auth::user()->crm_email;
        $this->apiKey = $crmKey ? $crmKey : Auth::user()->crm_key;
        $this->subDomain = env('CRM_SUBDOMAIN');
        $this->login();
    }

    public function login () {
        $url = 'https://'.$this->subDomain.'.amocrm.ru/private/api/auth.php?type=json';
        $params = [
            'USER_LOGIN' => $this->userName,
            'USER_HASH'  => $this->apiKey,
        ];
        $this->sendRequest($url, $params, 'POST');
    }

    public function addNoteToDeal ($text, $dealId) {
        $url = 'https://'.$this->subDomain.'.amocrm.ru/private/api/v2/json/notes/set';
        $notes['request']['notes']['add']=array(
            array(
                'element_id'=>$dealId,
                'element_type'=> 2,
                'note_type'=> 4,
                'text'=> $text,
            ),
        );
        return $this->sendRequest($url, $notes, 'POST');
    }

    public function updateDeal ($params) {
        $url = 'https://'.$this->subDomain.'.amocrm.ru/api/v2/json/leads/set';
        return $this->sendRequest($url, $params, 'POST');
    }

    public function fieldsList () {
        $url = 'https://'.$this->subDomain.'.amocrm.ru/api/v2/account';
        $params = ['with' => 'custom_fields'];
        return $this->sendRequest($url, $params, 'GET')->_embedded->custom_fields;
    }

    public function sendRequest ($url, $params, $method) {
        $curl=curl_init();
        curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
        curl_setopt($curl,CURLOPT_USERAGENT,'amoCRM-API-client/1.0');
//        curl_setopt($curl,CURLOPT_CUSTOMREQUEST, $method);
        if ($method === 'GET') {
            $url .= '?';
            foreach ($params as $paramName => $paramValue) {
                $url .= $paramName . '=' . $paramValue . '&';
            }
        } else if ($method === 'POST') {
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl,CURLOPT_POSTFIELDS, json_encode($params));
        }
        curl_setopt($curl, CURLOPT_ENCODING, '');
        curl_setopt($curl,CURLOPT_URL, $url);
        curl_setopt($curl,CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($curl,CURLOPT_HEADER,false);
        curl_setopt($curl,CURLOPT_COOKIEFILE, storage_path('cookies/' . $this->userName . '.txt'));
        curl_setopt($curl,CURLOPT_COOKIEJAR, storage_path('cookies/' . $this->userName . '.txt'));
        curl_setopt($curl,CURLOPT_SSL_VERIFYPEER,0);
        curl_setopt($curl,CURLOPT_SSL_VERIFYHOST,0);
        $out = curl_exec($curl);
        return json_decode($out);
    }
}

?>
