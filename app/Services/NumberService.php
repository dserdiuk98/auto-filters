<?php

namespace App\Services;

class NumberService
{
    public static function getNumberFromContact ($contactInfo) {
        $number = 0;
        foreach ($contactInfo['custom_fields'] as $customField) {
            if ($customField['id'] == 1427400) {
                $number = $customField['values'][0]['value'];
            }
        }
        return $number;
    }

    public static function phone($phone = '', $convert = true, $trim = true)
    {
        return preg_replace('~[^0-9]+~','',$phone);
    }

}

?>