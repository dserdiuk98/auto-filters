<?php

namespace App\Services;

class FerioService
{
    public static function sendToFerio($lead_id)
    {
        $z_zapch = '1758316'; //Поле с количеством запчастей для отправки

// Массив идентификаторами полей в amocrm
        $data_array = array(
            'Для запроса' => array(
                'Год выпуска' => '1758342',
                'Мощность' => '1758346',
                'Состояние' => '1758390'
            ),
            'Характеристики' => array(
                'Год выпуска' => '1747252',
                'Марка' => '1747248',
                'Модель' => '1747250',
                'Код кузова' => '1747592',
                'Период выпуска' => '1795414',
                'Рынок' => '1795416',
                'Комплектация' => '1747596',
                'Тип кузова' => '1747588',
                'Тип двигателя' => '1747570',
                'Объем' => '1747540',
                'Мощность' => '1747584',
                'Код двигателя' => '1747594',
                'КПП' => '1747590',
                'Код КПП' => '1747700',
                'Привод' => '1747586',
                'VIN' => '1747598'
            ),
            'Запчасти' => array(
                1 => array(
                    'Запчасть' => '1757498',
                    'Хештег' => '1757500',
                    'Доп. хештег' => '1757502',
                    'Исх. цена' => '1757506',
                    'Вход. цена' => '1757508',
                    'Наличие' => '1757510',
                    'Наличие доп' => '1757512'
                ),
                2 => array(
                    'Запчасть' => '1757518',
                    'Хештег' => '1757519',
                    'Доп. хештег' => '1757521',
                    'Исх. цена' => '1757525',
                    'Вход. цена' => '1757527',
                    'Наличие' => '1757529',
                    'Наличие доп' => '1757531'
                ),
                3 => array(
                    'Запчасть' => '1757535',
                    'Хештег' => '1757537',
                    'Доп. хештег' => '1757539',
                    'Исх. цена' => '1757543',
                    'Вход. цена' => '1757545',
                    'Наличие' => '1757547',
                    'Наличие доп' => '1757549'
                ),
                4 => array(
                    'Запчасть' => '1757555',
                    'Хештег' => '1757557',
                    'Доп. хештег' => '1757559',
                    'Исх. цена' => '1757563',
                    'Вход. цена' => '1757565',
                    'Наличие' => '1757567',
                    'Наличие доп' => '1757569'
                ),
                5 => array(
                    'Запчасть' => '1757656',
                    'Хештег' => '1757658',
                    'Доп. хештег' => '1757660',
                    'Исх. цена' => '1757662',
                    'Вход. цена' => '1757664',
                    'Наличие' => '1757666',
                    'Наличие доп' => '1757668'
                ),
                6 => array(
                    'Запчасть' => '1757672',
                    'Хештег' => '1757674',
                    'Доп. хештег' => '1757676',
                    'Исх. цена' => '1757682',
                    'Вход. цена' => '1757684',
                    'Наличие' => '1757686',
                    'Наличие доп' => '1757688'
                ),
                7 => array(
                    'Запчасть' => '1757692',
                    'Хештег' => '1757694',
                    'Доп. хештег' => '1757696',
                    'Исх. цена' => '1757700',
                    'Вход. цена' => '1757702',
                    'Наличие' => '1757704',
                    'Наличие доп' => '1757706'
                ),
                8 => array(
                    'Запчасть' => '1757710',
                    'Хештег' => '1757712',
                    'Доп. хештег' => '1757714',
                    'Исх. цена' => '1757718',
                    'Вход. цена' => '1757720',
                    'Наличие' => '1757722',
                    'Наличие доп' => '1757724'
                ),
                9 => array(
                    'Запчасть' => '1757728',
                    'Хештег' => '1757730',
                    'Доп. хештег' => '1757732',
                    'Исх. цена' => '1757736',
                    'Вход. цена' => '1757738',
                    'Наличие' => '1757740',
                    'Наличие доп' => '1757742'
                ),
                10 => array(
                    'Запчасть' => '1757748',
                    'Хештег' => '1757750',
                    'Доп. хештег' => '1757752',
                    'Исх. цена' => '1757756',
                    'Вход. цена' => '1757758',
                    'Наличие' => '1757760',
                    'Наличие доп' => '1757762'
                ),
                11 => array(
                    'Запчасть' => '1759356',
                    'Хештег' => '1759358',
                    'Доп. хештег' => '1759360',
                    'Исх. цена' => '1759362',
                    'Вход. цена' => '1759364',
                    'Наличие' => '1759366',
                    'Наличие доп' => '1759368'
                ),
                12 => array(
                    'Запчасть' => '1759376',
                    'Хештег' => '1759378',
                    'Доп. хештег' => '1759380',
                    'Исх. цена' => '1759382',
                    'Вход. цена' => '1759384',
                    'Наличие' => '1759386',
                    'Наличие доп' => '1759388'
                )
            ),
            'Ссылки' => array(
                1 => array(
                    'value' => '1757774'
                ),
                2 => array(
                    'value' => '1757776'
                ),
                3 => array(
                    'value' => '1757778'
                ),
                4 => array(
                    'value' => '1757780'
                ),
                5 => array(
                    'value' => '1757782'
                ),
                6 => array(
                    'value' => '1757784'
                ),
                7 => array(
                    'value' => '1757786'
                )
            )
        );
//        $lead_id = (int)49466374;


        $error = '';

// Данные для авторизации в amocrm
        $user = array(
            'USER_LOGIN' => 'allregdata@yandex.ru', #Ваш логин (электронная почта)
            'USER_HASH' => '1009ec0720aeccca0cad92f92e418a163ae892a2' #Хэш для доступа к API (смотрите в профиле пользователя)
        );
        $subdomain = 'estzapchasti'; #Наш аккаунт - поддомен


// Авторизация в amocrm
        $link = 'https://' . $subdomain . '.amocrm.ru/private/api/auth.php?type=json';
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_USERAGENT, 'amoCRM-API-client/1.0');
        curl_setopt($curl, CURLOPT_URL, $link);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($user));
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_COOKIEFILE, storage_path('cookies/server.txt'));
        curl_setopt($curl, CURLOPT_COOKIEJAR, storage_path('cookies/server.txt'));
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        $out = curl_exec($curl);
        $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);


// Получаем данные сделки
        $link = 'https://' . $subdomain . '.amocrm.ru/private/api/v2/json/leads/list?id=' . $lead_id;
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_USERAGENT, 'amoCRM-API-client/1.0');
        curl_setopt($curl, CURLOPT_URL, $link);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_COOKIEFILE, storage_path('cookies/server.txt'));
        curl_setopt($curl, CURLOPT_COOKIEJAR, storage_path('cookies/server.txt'));
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        $out = curl_exec($curl);
        $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        $out = json_decode($out, true);

        $lead_info = $out['response']['leads'][0];

        $custom_field = $lead_info['custom_fields'];

        $year = '';
        $year_zap = '';
        $brand_name = '';
        $model_name = '';
        $body_code = '';
        $body_id = '';
        $engine_type_id = '';
        $engine_vol = '';
        $horsepower = '';
        $horsepower_zap = '';
        $engine_code = '';
        $gearbox_id = '';
        $drive_id = '';
        $vin = '';
        $type = '';
        $col_field = count($custom_field);
        $col_zapch = count($data_array['Запчасти']);
        $col_href = count($data_array['Ссылки']);
        $col_ferio_zapch = '';
        $parts = array();
        $last_ind = 0;

// Получаем из amocrm нужные данные
        for ($i = 0; $i < $col_field; $i++) {
            if ($custom_field[$i]['id'] == $data_array['Для запроса']['Год выпуска']) {
                $year_zap = $custom_field[$i]['values'][0]['value']; //Год выпуска для запроса
            } else if ($custom_field[$i]['id'] == $data_array['Для запроса']['Мощность']) {
                $horsepower_zap = (int)$custom_field[$i]['values'][0]['value']; //Мощность для запроса
            } else if ($custom_field[$i]['id'] == $data_array['Для запроса']['Состояние']) {
                $type = $custom_field[$i]['values'][0]['value']; //Состояние для запроса
                if ($type == 'Б/У') {
                    $type = 2;
                } else if ($type == 'Б/У или Новый') {
                    $type = 1;
                } else if ($type == 'Новый') {
                    $type = 3;
                }
            } else if ($custom_field[$i]['id'] == $data_array['Характеристики']['Год выпуска']) {
                $year = $custom_field[$i]['values'][0]['value'];
            } else if ($custom_field[$i]['id'] == $data_array['Характеристики']['Марка']) {
                $brand_name = $custom_field[$i]['values'][0]['value'];
            } else if ($custom_field[$i]['id'] == $data_array['Характеристики']['Модель']) {
                $model_name = $custom_field[$i]['values'][0]['value'];
            } else if ($custom_field[$i]['id'] == $data_array['Характеристики']['Код кузова']) {
                $body_code = $custom_field[$i]['values'][0]['value'];
            } else if ($custom_field[$i]['id'] == $data_array['Характеристики']['Период выпуска']) {
                $body_code .= strlen($custom_field[$i]['values'][0]['value']) > 0 ? " " . $custom_field[$i]['values'][0]['value'] : "";
            } else if ($custom_field[$i]['id'] == $data_array['Характеристики']['Рынок']) {
                $body_code .= strlen($custom_field[$i]['values'][0]['value']) > 0 ? " " . $custom_field[$i]['values'][0]['value'] : "";
            } else if ($custom_field[$i]['id'] == $data_array['Характеристики']['Тип кузова']) {
                $body_name = $custom_field[$i]['values'][0]['value'];
                $data = array(
                    array(
                        "name" => "Автобус (более 8 мест)",
                        "value" => "Bus"
                    ),
                    array(
                        "name" => "Внедорожник",
                        "value" => "SUV"
                    ),
                    array(
                        "name" => "Внедорожник (3 дв.)",
                        "value" => "SUV-3"
                    ),
                    array(
                        "name" => "Каблук",
                        "value" => "Panel Van"
                    ),
                    array(
                        "name" => "Кабриолет",
                        "value" => "Cabrio"
                    ),
                    array(
                        "name" => "Кроссовер",
                        "value" => "CUV"
                    ),
                    array(
                        "name" => "Кроссовер (3 дв.)",
                        "value" => "CUV-3"
                    ),
                    array(
                        "name" => "Купе",
                        "value" => "Coupe"
                    ), array(
                        "name" => "Минивен",
                        "value" => "Minivan"
                    ),
                    array(
                        "name" => "Пикап",
                        "value" => "Pickup"
                    ),
                    array(
                        "name" => "Платформа (коммерч.)",
                        "value" => "Truck"
                    ),
                    array(
                        "name" => "Родстер",
                        "value" => "Rodster"
                    ),
                    array(
                        "name" => "Таун Кар",
                        "value" => "Town_Car"
                    ),
                    array(
                        "name" => "Седан",
                        "value" => "Sedan"
                    ),
                    array(
                        "name" => "Универсал",
                        "value" => "Wagon"
                    ),
                    array(
                        "name" => "Фургон",
                        "value" => "VAN"
                    ),
                    array(
                        "name" => "Хетчбэк (5 дв.)",
                        "value" => "Hatchback"
                    ),
                    array(
                        "name" => "Хетчбэк (3 дв.)",
                        "value" => "Hatchback-3"
                    )
                );
                $col = count($data);
                for ($j = 0; $j < $col; $j++) {
                    if ($data[$j]['name'] == $body_name) {
                        $body_id = $data[$j]['value'];
                        break 1;
                    }
                }
            } else if ($custom_field[$i]['id'] == $data_array['Характеристики']['Тип двигателя']) {
                $engine_type_name = $custom_field[$i]['values'][0]['value'];
                $data = array(
                    array(
                        "name" => "Бензин",
                        "value" => "Petrol"
                    ),
                    array(
                        "name" => "Дизель",
                        "value" => "Diesel"
                    ),
                    array(
                        "name" => "Гибрид",
                        "value" => "Hybrid"
                    ),
                    array(
                        "name" => "Газ",
                        "value" => "Gus"
                    ),
                    array(
                        "name" => "Электричество",
                        "value" => "Elec"
                    ),
                    array(
                        "name" => "Биотопливо",
                        "value" => "Bio"
                    ),
                    array(
                        "name" => "Flex-Fuel",
                        "value" => "Flex-Fuel"
                    )
                );
                $col = count($data);
                for ($j = 0; $j < $col; $j++) {
                    if ($data[$j]['name'] == $engine_type_name) {
                        $engine_type_id = $data[$j]['value'];
                        break 1;
                    }
                }

            } else if ($custom_field[$i]['id'] == $data_array['Характеристики']['Объем']) {
                $engine_vol = $custom_field[$i]['values'][0]['value'];
            } else if ($custom_field[$i]['id'] == $data_array['Характеристики']['Мощность']) {
                $horsepower = (int)$custom_field[$i]['values'][0]['value'];
            } else if ($custom_field[$i]['id'] == $data_array['Характеристики']['Код двигателя']) {
                $engine_code = 'ДВС - ' . $custom_field[$i]['values'][0]['value'];
            } else if ($custom_field[$i]['id'] == $data_array['Характеристики']['Код КПП']) {
                if (!empty($engine_code)) {
                    $engine_code = $engine_code . ' | КПП - ' . $custom_field[$i]['values'][0]['value'];
                } else {
                    $engine_code = 'КПП - ' . $custom_field[$i]['values'][0]['value'];
                }
            } else if ($custom_field[$i]['id'] == $data_array['Характеристики']['КПП']) {
                $gearbox_name = $custom_field[$i]['values'][0]['value'];
                $data = array(
                    array(
                        "name" => "Автомат",
                        "value" => "AT"
                    ),
                    array(
                        "name" => "Вариатор",
                        "value" => "CVT"
                    ),
                    array(
                        "name" => "Механика",
                        "value" => "MT"
                    ),
                    array(
                        "name" => "Типтроник",
                        "value" => "TT"
                    )
                );
                $col = count($data);
                for ($j = 0; $j < $col; $j++) {
                    if ($data[$j]['name'] == $gearbox_name) {
                        $gearbox_id = $data[$j]['value'];
                        break 1;
                    }
                }
            } else if ($custom_field[$i]['id'] == $data_array['Характеристики']['Привод']) {
                $drive_name = $custom_field[$i]['values'][0]['value'];
                $data = array(
                    array(
                        "name" => "Задний",
                        "value" => "RWD"
                    ),
                    array(
                        "name" => "Передний",
                        "value" => "FWD"
                    ),
                    array(
                        "name" => "Полный",
                        "value" => "4WD"
                    )
                );
                $col = count($data);
                for ($j = 0; $j < $col; $j++) {
                    if ($data[$j]['name'] == $drive_name) {
                        $drive_id = $data[$j]['value'];
                        break 1;
                    }
                }
            } else if ($custom_field[$i]['id'] == $data_array['Характеристики']['VIN']) {
                $vin = $custom_field[$i]['values'][0]['value'];
            } // Определяем какие запчасти отправлять
            else if ($custom_field[$i]['id'] == $z_zapch) {
                if ($custom_field[$i]['values'][0]['value'] == 'Все запчасти') {
                    $col_ferio_zapch = 'all';
                } else {
                    $col = count($custom_field[$i]['values']);
                    for ($j = 0; $j < $col; $j++) {
                        if (empty($col_ferio_zapch)) {
                            $col_ferio_zapch = $custom_field[$i]['values'][$j]['value'];
                        } else {
                            $col_ferio_zapch = $col_ferio_zapch . ',' . $custom_field[$i]['values'][$j]['value'];
                        }
                    }
                }
            } else {
                // Получаем данные запчастей
                for ($j = 0; $j <= $col_zapch; $j++) {
                    if ($j > 0) {
                        if ($custom_field[$i]['id'] == $data_array['Запчасти'][$j]['Запчасть']) {
                            $parts[$j]['name'] = $custom_field[$i]['values'][0]['value'];
                            break 1;
                        } else if ($custom_field[$i]['id'] == $data_array['Запчасти'][$j]['Хештег']) {
                            $col = count($custom_field[$i]['values']);
                            for ($k = 0; $k < $col; $k++) {
                                if (empty($parts[$j]['hashtag'])) {
                                    $parts[$j]['hashtag'] = $custom_field[$i]['values'][$k]['value'];
                                } else {
                                    $parts[$j]['hashtag'] = $parts[$j]['hashtag'] . ', ' . $custom_field[$i]['values'][$k]['value'];
                                }
                            }
                            break 1;
                        } else if ($custom_field[$i]['id'] == $data_array['Запчасти'][$j]['Доп. хештег']) {
                            $parts[$j]['dop_hashtag'] = $custom_field[$i]['values'][0]['value'];
                            break 1;
                        }

                        if (empty($parts[$j]['name'])) {
                            unset($parts[$j]);
                        }
                    }
                }
                // Ссылки
                for ($k = 0; $k <= $col_href; $k++) {
                    if ($k > 0) {
                        if ($custom_field[$i]['id'] == $data_array['Ссылки'][$k]['value']) {
                            if ($last_ind < $k) {
                                $last_ind = $k;
                            }
                            break 1;
                        }
                    }
                }

            }


        }

        $er = 0;
        if (empty($year) && empty($year_zap)) {
            $error .= "Не указан год.<br>";
            $er = 1;
        }
        if (empty($brand_name)) {
            $error .= "Не указана марка.<br>";
            $er = 1;
        }
        if (empty($model_name)) {
            $error .= "Не указана модель.<br>";
            $er = 1;
        }

        if ($er == 1) {
            echo $error;
            exit();
        }

//print_r($data_array);
//exit;


// Фильтруем запчасти, которые нужны для отправки
        if (empty($col_ferio_zapch)) {
            $col_ferio_zapch = 'all';
        }
        if ($col_ferio_zapch !== 'all') {
            $col_ferio_zapch = explode(",", $col_ferio_zapch);
        }

        $col = count($parts);
        $p = 0;

        foreach ($parts as $key => $value) {
            $p = $p + 1;
            if ($col_ferio_zapch == 'all') {
                if (!empty($value['hashtag']) && !empty($value['dop_hashtag'])) {
                    $ferio_parts[$p]['name'] = $value['name'] . ' - ' . $value['hashtag'] . ', ' . $value['dop_hashtag'];
                } else if (!empty($value['hashtag'])) {
                    $ferio_parts[$p]['name'] = $value['name'] . ' - ' . $value['hashtag'];
                } else if (!empty($value['dop_hashtag'])) {
                    $ferio_parts[$p]['name'] = $value['name'] . ' - ' . $value['dop_hashtag'];
                } else {
                    $ferio_parts[$p]['name'] = $value['name'];
                }

                if (!empty($type)) {
                    $ferio_parts[$p]['type'] = $type;
                } else {
                    $ferio_parts[$p]['type'] = 1;
                }
            } else {
                $col2 = count($col_ferio_zapch);
                for ($j = 0; $j < $col2; $j++) {
                    $col_ferio_zapch[$j] = (int)$col_ferio_zapch[$j];

                    if ($key == $col_ferio_zapch[$j]) {
                        if (!empty($value['hashtag']) && !empty($value['dop_hashtag'])) {
                            $ferio_parts[$p]['name'] = $value['name'] . ' - ' . $value['hashtag'] . ', ' . $value['dop_hashtag'];
                        } else if (!empty($value['hashtag'])) {
                            $ferio_parts[$p]['name'] = $value['name'] . ' - ' . $value['hashtag'];
                        } else if (!empty($value['dop_hashtag'])) {
                            $ferio_parts[$p]['name'] = $value['name'] . ' - ' . $value['dop_hashtag'];
                        } else {
                            $ferio_parts[$p]['name'] = $value['name'];
                        }

                        if (!empty($type)) {
                            $ferio_parts[$p]['type'] = $type;
                        } else {
                            $ferio_parts[$p]['type'] = 1;
                        }
                    }
                }
            }

        }

        if (count($ferio_parts) == 0) {
            $error .= "Не заполнено поле запчастей<br>";
            echo $error;
            exit();
        }

// Авторизуемся на ferio
        $user = array(
            'email' => 'mr.maxego@gmail.com',
            'password' => 'Dd12345678'
        );
        $link = 'http://api.ferio.ru/Display/modules/mobileAPI.responce.php?action=login';
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_URL, $link);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($user));
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_COOKIEFILE, storage_path('cookies/server.txt'));
        curl_setopt($curl, CURLOPT_COOKIEJAR, storage_path('cookies/server.txt'));
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        $out = curl_exec($curl);
        $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        $out = json_decode($out, true);
        $phpsessid = $out['phpsessid'];


        if (!empty($year_zap)) {
            $year = $year_zap;
        }

//Получение списка брендов по году
        $data = array(
            'year' => $year
        );
        $link = 'http://api.ferio.ru/Display/modules/mobileAPI.responce.php?action=getBrandByYearJSON';
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_URL, $link);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_COOKIEFILE, storage_path('cookies/server.txt'));
        curl_setopt($curl, CURLOPT_COOKIEJAR, storage_path('cookies/server.txt'));
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        $out = curl_exec($curl);
        $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        $out = json_decode($out, true);
        $col_brand = count($out);
        for ($i = 0; $i < $col_brand; $i++) {
            if ($out[$i]['name'] == $brand_name) {
                $brand_id = $out[$i]['brand_id'];
            }
        }

        if (empty($brand_id)) {
            $error .= "Марки с таким названием не нашлось.<br>";
            echo $error;
            exit();
        }

// Получаем Id модели
        $data = array(
            'year' => $year,
            'brand_id' => $brand_id
        );
        $link = 'http://api.ferio.ru/Display/modules/mobileAPI.responce.php?action=getModelByYearBrandJSON';
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_URL, $link);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_COOKIEFILE, storage_path('cookies/server.txt'));
        curl_setopt($curl, CURLOPT_COOKIEJAR, storage_path('cookies/server.txt'));
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        $out = curl_exec($curl);
        $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        $out = json_decode($out, true);

        $col_model = count($out);
        $a = 1;
        for ($i = 0; $i < $col_model; $i++) {
            $model_name2 = '';
            if ($out[$i]['name'] == $model_name) {
                $model_id = (int)$out[$i]['model_id'];
            }

            if (empty($model_id)) {
                $model_name_ar = explode(' ', $model_name);
                $col = count($model_name_ar);
                for ($j = 0; $j < $col; $j++) {
                    if ($j !== ($col - $a)) {
                        $model_name2 .= $model_name_ar[$j];
                        if ($out[$i]['name'] == $model_name2) {
                            $model_id = $out[$i]['model_id'];
                            if (empty($model_id)) {
                                $a = $a + 1;
                                if ($a < $col) {
                                    continue;
                                } else {
                                    break 2;
                                }
                            }
                        }
                    }
                }
            }

        }
        $err_model = '';
        if (empty($model_id)) {
            for ($i = 0; $i < $col_model; $i++) {
                if (empty($err_model)) {
                    $err_model = $out[$i]['name'];
                } else {
                    $err_model = $err_model . ', ' . $out[$i]['name'];
                }
            }
            $error .= "Модели с таким названием не нашлось.<br>Доступные модели для данной марки:<br>" . $err_model . "<br>";
            echo $error;
            exit();
        }

        if (empty($engine_vol)) {
            $engine_vol = '1.6';
        }
        if (empty($engine_type_id)) {
            $engine_type_id = 'Petrol';
        }
        if (empty($drive_id)) {
            $drive_id = 'FWD';
        }
        if (empty($body_id)) {
            $body_id = 'Hatchback';
        }
        if (empty($gearbox_id)) {
            $gearbox_id = 'AT';
        }
        if (empty($horsepower) && !empty($horsepower_zap)) {
            $horsepower = $horsepower_zap;
        } else if (!empty($horsepower) && empty($horsepower_zap)) {
            $horsepower = $horsepower;
        } else if (!empty($horsepower) && !empty($horsepower_zap)) {
            $horsepower = $horsepower;
        } else {
            $horsepower = 102;
        }


// Отправка запроса с запчастями
        $data = array(
            'phpsessid' => $phpsessid, //Идентификатор сессии***
            'year' => $year, //Год***
            'model_id' => $model_id, // Модель***
            'engine_vol' => $engine_vol, //Объем двигателя***
            'engine_type_id' => $engine_type_id, //Тип двигателя***
            'horsepower' => $horsepower, //Мощность двигателя***
            'drive_id' => $drive_id, //Привод автомобиля***
            'body_id' => $body_id, //Тип кузова***
            'gearbox_id' => $gearbox_id,    //КПП***
            'city_id' => 2097, //Город***
            'parts' => $ferio_parts, //Массив запчастей***

            'body_code' => $body_code, //Код кузова
            'engine_code' => $engine_code, //Код двигателя
            'market' => '', //Рынок
            'vin' => $vin //VIN(FRAME)
        );
        $data_string = http_build_query($data);
        $link = 'http://api.ferio.ru/Display/modules/mobileAPI.responce.php?action=addRequestJSON';
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_URL, $link);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_COOKIEFILE, storage_path('cookies/server.txt'));
        curl_setopt($curl, CURLOPT_COOKIEJAR, storage_path('cookies/server.txt'));
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        $out = curl_exec($curl);
        $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        $out = json_decode($out, true);
        if ($out['result'] == 'true') {
            echo 'OK';

            $href_zapros = 'http://partner.est-zapchasti.ru/?order_id=' . $out['request_id'];
            $ferio_order_id = (int)$out['request_id'];

            if ($last_ind < 7) {
                $id_custom = (int)$data_array['Ссылки'][$last_ind + 1]['value'];
            } else {
                $id_custom = (int)$data_array['Ссылки'][$last_ind]['value'];
            }
            // Добавляем в сделку ссылку на запрос
            $leads['request']['leads']['update'] = array(
                array(
                    'id' => $lead_id,
                    'last_modified' => time(),
                    'custom_fields' => array(
                        array(
                            'id' => $id_custom,
                            'values' => array(
                                array(
                                    'value' => $href_zapros
                                )
                            )
                        )
                    )
                )
            );
            $link = 'https://' . $subdomain . '.amocrm.ru/private/api/v2/json/leads/set';
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_USERAGENT, 'amoCRM-API-client/1.0');
            curl_setopt($curl, CURLOPT_URL, $link);
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
            curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($leads));
            curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
            curl_setopt($curl, CURLOPT_HEADER, false);
            curl_setopt($curl, CURLOPT_COOKIEFILE, storage_path('cookies/server.txt'));
            curl_setopt($curl, CURLOPT_COOKIEJAR, storage_path('cookies/server.txt'));
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
            $out = curl_exec($curl);
            $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            $out = json_decode($out, true);
            if (empty($out['response']['leads']['update']['id'])) {
                sleep(1);
                $link = 'https://' . $subdomain . '.amocrm.ru/private/api/v2/json/leads/set';
                $curl = curl_init();
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($curl, CURLOPT_USERAGENT, 'amoCRM-API-client/1.0');
                curl_setopt($curl, CURLOPT_URL, $link);
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
                curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($leads));
                curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
                curl_setopt($curl, CURLOPT_HEADER, false);
                curl_setopt($curl, CURLOPT_COOKIEFILE, storage_path('cookies/server.txt'));
                curl_setopt($curl, CURLOPT_COOKIEJAR, storage_path('cookies/server.txt'));
                curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
                curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
                $out = curl_exec($curl);
                $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
                $out = json_decode($out, true);
                if (empty($out['response']['leads']['update']['id'])) {
                    $date = date('d.m.Y H:i:s');
                    $status = 0;
//                    $db = new SafeMySQL();
//                    $sql = "INSERT IGNORE INTO " . DB_TABLE_FERIO . " (lead_id, date, ferio_order_id, status) VALUES (?i, ?s, ?s, ?i)";
//                    $response = $db->query($sql, $lead_id, $date, $ferio_order_id, $status);

                }
            }
        } else {
            if (empty($error)) {
                echo 'Произошла какая то ошибка';
            } else {
                echo $error;
            }
        }
    }

    public static $mistMarks = array (
        '105/115' => 'Alfa Romeo',
        1900 => 'Alfa Romeo',
        2600 => 'Alfa Romeo',
        6 => 'Renault',
        '6C' => 'Alfa Romeo',
        '8C Competizione' => 'Alfa Romeo',
        'Alfasud' => 'Alfa Romeo',
        'Arna' => 'Alfa Romeo',
        'Disco Volante' => 'Alfa Romeo',
        'GTA Coupe' => 'Alfa Romeo',
        'Montreal' => 'Alfa Romeo',
        'Sprint' => 'Alfa Romeo',
        'SZ' => 'Alfa Romeo',
        'HMMWV (Humvee)' => 'AM General',
        'Hornet' => 'AMC',
        'Bulldog' => 'Aston Martin',
        'DB5' => 'Aston Martin',
        'DB AR1' => 'Aston Martin',
        'V12 Vantage' => 'Aston Martin',
        'V12 Zagato' => 'Aston Martin',
        'V8 Vantage' => 'Aston Martin',
        'V8 Zagato' => 'Aston Martin',
        50 => 'Audi',
        920 => 'Audi',
        'A4 allroad' => 'Audi',
        'A6 allroad' => 'Audi',
        'e-tron' => 'Audi',
        'NSU RO 80' => 'Audi',
        'Q8' => 'Audi',
        'Quattro' => 'Audi',
        'RS 2' => 'Audi',
        'RS 3' => 'Audi',
        'RS 4' => 'Audi',
        'RS 5' => 'Audi',
        'RS 6' => 'Audi',
        'RS 7' => 'Audi',
        'RS Q3' => 'Audi',
        'S1' => 'Audi',
        'S2' => 'Audi',
        'S7' => 'Audi',
        'SQ2' => 'Audi',
        'SQ5' => 'Audi',
        'SQ7' => 'Audi',
        'TT RS' => 'Audi',
        'TTS' => 'Audi',
        'Typ R' => 'Audi',
        'Continental' => 'Bentley',
        'Eight' => 'Mercury',
        'Flying Spur' => 'Bentley',
        'Mark VI' => 'Bentley',
        'R Type' => 'Bentley',
        'S' => 'Bentley',
        'T-Series' => 'Bentley',
        'Turbo R' => 'Bentley',
        '02 (E10)' => 'BMW',
        '1 серия' => 'BMW',
        '2000 C/CS' => 'BMW',
        '2 серия Active Tourer' => 'BMW',
        '2 серия' => 'BMW',
        '2 серия Grand Tourer' => 'BMW',
        315 => 'BMW',
        3200 => 'BMW',
        321 => 'BMW',
        326 => 'BMW',
        327 => 'BMW',
        340 => 'BMW',
        '3/15' => 'BMW',
        '3 серия' => 'BMW',
        '4 серия' => 'BMW',
        501 => 'BMW',
        502 => 'BMW',
        503 => 'BMW',
        507 => 'BMW',
        '5 серия' => 'BMW',
        600 => 'Dodge',
        '6 серия' => 'BMW',
        700 => 'BMW',
        '7 серия' => 'BMW',
        '8 серия' => 'BMW',
        'E3' => 'BMW',
        'E9' => 'BMW',
        'i3' => 'BMW',
        '1M' => 'BMW',
        'M2' => 'BMW',
        'New Class' => 'BMW',
        'X5 M' => 'BMW',
        'X6 M' => 'BMW',
        'X7' => 'BMW',
        'Z1' => 'BMW',
        'Z3' => 'BMW',
        'Z3 M' => 'BMW',
        'Z4' => 'BMW',
        'Z4 M' => 'BMW',
        'Z8' => 'BMW',
        '7.3S' => 'Brabus',
        'M V12' => 'Brabus',
        'ML 63 Biturbo' => 'Brabus',
        'SV12' => 'Brabus',
        'FRV (BS2)' => 'Brilliance',
        'H230' => 'Brilliance',
        'M1 (BS6)' => 'Brilliance',
        'M2 (BS4)' => 'Brilliance',
        'M3 (BC3)' => 'Brilliance',
        'Chiron' => 'Bugatti',
        'EB 110' => 'Bugatti',
        'EB 112' => 'Bugatti',
        'EB Veyron 16.4' => 'Bugatti',
        'Type 55' => 'Bugatti',
        'Electra' => 'Buick',
        'Encore' => 'Buick',
        'Envision' => 'Buick',
        'Estate Wagon' => 'Buick',
        'Excelle' => 'Buick',
        'GL8' => 'Buick',
        'GS' => 'MG',
        'LaCrosse' => 'Buick',
        'LeSabre' => 'Buick',
        'Limited' => 'Buick',
        'Rainer' => 'Buick',
        'Reatta' => 'Buick',
        'Skyhawk' => 'Buick',
        'Special' => 'Buick',
        'Super' => 'Buick',
        'Wildcat' => 'Daihatsu',
        'E6' => 'BYD',
        'F5' => 'BYD',
        'F8 (S8)' => 'BYD',
        'G3' => 'BYD',
        'G6' => 'BYD',
        'L3' => 'BYD',
        'M6' => 'BYD',
        'S6' => 'BYD',
        'Allante' => 'Cadillac',
        'ATS-V' => 'Cadillac',
        'Brougham' => 'Cadillac',
        'CT6' => 'Cadillac',
        'CTS-V' => 'Cadillac',
        'DeVille' => 'Cadillac',
        'ELR' => 'Cadillac',
        'LSE' => 'Cadillac',
        'Series 62' => 'Cadillac',
        'Sixty Special' => 'Cadillac',
        'XTS' => 'Cadillac',
        'Amulet (A15)' => 'Chery',
        'Arrizo 7' => 'Chery',
        'Arrizo 3' => 'Chery',
        'B13' => 'Chery',
        'Bonus (A13)' => 'Chery',
        'Bonus 3 (E3/A19)' => 'Chery',
        'CrossEastar (B14)' => 'Chery',
        'Fora (A21)' => 'Chery',
        'IndiS (S18D)' => 'Chery',
        'Kimo (A1)' => 'Chery',
        'M11 (A3)' => 'Chery',
        'Oriental Son (B11)' => 'Chery',
        'Sweet (QQ)' => 'Chery',
        'QQ6 (S21)' => 'Chery',
        'QQme' => 'Chery',
        'Tiggo (T11)' => 'Chery',
        'Tiggo 2' => 'Chery',
        'Tiggo 3' => 'Chery',
        'Tiggo 5' => 'Chery',
        'Very (A13)' => 'Chery',
        'Windcloud (A11)' => 'Chery',
        'Astra' => 'Vauxhall',
        'Astro' => 'Chevrolet',
        'Bel Air' => 'Chevrolet',
        'Bolt' => 'Chevrolet',
        'C-10' => 'Chevrolet',
        'Celta' => 'Chevrolet',
        'Chevelle' => 'Chevrolet',
        'Chevette' => 'Vauxhall',
        'Citation' => 'Chevrolet',
        'C/K' => 'Chevrolet',
        'Classic' => 'Toyota',
        'Matiz' => 'Chevrolet',
        'Corsa' => 'Chevrolet',
        'Corvair' => 'Chevrolet',
        'Cruze (HR)' => 'Chevrolet',
        'Deluxe' => 'Chevrolet',
        'Fleetmaster' => 'Chevrolet',
        'Blazer K5' => 'Chevrolet',
        'Kalos' => 'Chevrolet',
        'Lumina APV' => 'Chevrolet',
        'LUV D-MAX' => 'Chevrolet',
        'Master' => 'Chevrolet',
        'Monza' => 'Chevrolet',
        'MW' => 'Chevrolet',
        'Nubira' => 'Chevrolet',
        'Omega' => 'Vauxhall',
        'S-10 Pickup' => 'Chevrolet',
        'Sail' => 'Chevrolet',
        'Special DeLuxe' => 'Chevrolet',
        'SS' => 'Chevrolet',
        'Starcraft' => 'Chevrolet',
        'Tavera' => 'Chevrolet',
        'Trans Sport' => 'Chevrolet',
        'Traverse' => 'Chevrolet',
        'Zafira' => 'Holden',
        180 => 'Chrysler',
        200 => 'Chrysler',
        300 => 'Chrysler',
        '300 Letter Series' => 'Chrysler',
        'Aspen' => 'Chrysler',
        'Cordoba' => 'Chrysler',
        'Dynasty' => 'Hyundai',
        'Imperial Crown' => 'Chrysler',
        'LeBaron' => 'Chrysler',
        'Nassau' => 'Chrysler',
        'Newport' => 'Chrysler',
        'TC by Maserati' => 'Chrysler',
        'Town &amp; Country' => 'Chrysler',
        'Windsor' => 'Chrysler',
        '2 CV' => 'Citroen',
        'AMI' => 'Citroen',
        'AX' => 'Citroen',
        'C3 Aircross' => 'Citroen',
        'C4 Cactus' => 'Citroen',
        'C4 SpaceTourer' => 'Citroen',
        'C5 Aircross' => 'Citroen',
        'C-Quatre' => 'Citroen',
        'C-Triomphe' => 'Citroen',
        'C-ZERO' => 'Citroen',
        'CX' => 'Citroen',
        'DS' => 'Citroen',
        'Dyane' => 'Citroen',
        'E-Mehari' => 'Citroen',
        'LN' => 'Citroen',
        'Nemo' => 'Citroen',
        'SM' => 'Citroen',
        'Traction Avant' => 'Citroen',
        'Visa' => 'Citroen',
        'XM' => 'Citroen',
        1300 => 'Mazda',
        1310 => 'Dacia',
        1325 => 'Dacia',
        1410 => 'Dacia',
        'Dokker' => 'Renault',
        'Duster' => 'Dacia',
        'Lodgy' => 'Renault',
        'Logan' => 'Dacia',
        'Nova' => 'Dacia',
        'Pick-Up' => 'Dacia',
        'Sandero' => 'Dacia',
        'Solenza' => 'Dacia',
        'SuperNova' => 'Dacia',
        'Alpheon' => 'Daewoo',
        'Arcadia' => 'Daewoo',
        'Chairman' => 'Daewoo',
        'G2X' => 'Daewoo',
        'Lacetti Premiere' => 'Daewoo',
        'Matiz Creative' => 'Daewoo',
        'Musso' => 'Daewoo',
        'Racer' => 'Daewoo',
        'Royale' => 'Vauxhall',
        'Sens' => 'Daewoo',
        'Bee' => 'Daihatsu',
        'Ceria' => 'Daihatsu',
        'Fellow' => 'Daihatsu',
        'Midget' => 'MG',
        'Mira e:S' => 'Daihatsu',
        'Move Canbus' => 'Daihatsu',
        'Taft' => 'Daihatsu',
        'Xenia' => 'Daihatsu',
        'DS420' => 'Daimler',
        'Sovereign (XJ6)' => 'Daimler',
        'SP250' => 'Daimler',
        'X300' => 'Daimler',
        'X308' => 'Daimler',
        'X350' => 'Daimler',
        'XJ40' => 'Daimler',
        'XJS' => 'Daimler',
        '240Z' => 'Datsun',
        '280ZX' => 'Nissan',
        720 => 'Datsun',
        'Bluebird' => 'Datsun',
        'Cherry' => 'Datsun',
        'GO' => 'Datsun',
        'GO+' => 'Datsun',
        'Laurel' => 'Datsun',
        'Stanza' => 'Datsun',
        'Sunny' => 'Datsun',
        'Urvan' => 'Datsun',
        'Violet' => 'Datsun',
        'Aries' => 'Dodge',
        'Charger Daytona' => 'Dodge',
        'Custom Royal' => 'Dodge',
        'D8' => 'Dodge',
        'Diplomat' => 'Opel',
        'D/W Series' => 'Dodge',
        'Lancer' => 'Dodge',
        'Mayfair' => 'Dodge',
        'Monaco' => 'Dodge',
        'Omni' => 'Dodge',
        'Raider' => 'Dodge',
        'Ramcharger' => 'Dodge',
        'WC series' => 'Dodge',
        370 => 'DongFeng',
        'A30' => 'DongFeng',
        'A9' => 'DongFeng',
        'AX7' => 'DongFeng',
        'H30 Cross' => 'DongFeng',
        'MPV' => 'DongFeng',
        'Oting' => 'DongFeng',
        'Rich' => 'DongFeng',
        'S30' => 'DongFeng',
        'H3' => 'DW Hower',
        'H5' => 'DW Hower',
        'Jinn' => 'FAW',
        'Oley' => 'FAW',
        'Vita' => 'Opel',
        'Besturn X80' => 'FAW',
        'Dino 208/308 GT4' => 'Ferrari',
        '250 GTO' => 'Ferrari',
        348 => 'Ferrari',
        360 => 'Subaru',
        400 => 'Ferrari',
        412 => 'Ferrari',
        456 => 'Ferrari',
        '512 BB' => 'Ferrari',
        '512 TR' => 'Ferrari',
        '512 M' => 'Ferrari',
        550 => 'MG',
        612 => 'Ferrari',
        '812 Superfast' => 'Ferrari',
        'Dino 206 GT' => 'Ferrari',
        'Dino 246 GT' => 'Ferrari',
        'Enzo' => 'Ferrari',
        'F12berlinetta' => 'Ferrari',
        'F40' => 'Ferrari',
        'FXX K' => 'Ferrari',
        'LaFerrari' => 'Ferrari',
        'Mondial' => 'Ferrari',
        'Portofino' => 'Ferrari',
        'Testarossa' => 'Ferrari',
        124 => 'Fiat',
        '124 Spider' => 'Fiat',
        '124 Sport Spider' => 'Fiat',
        126 => 'Fiat',
        127 => 'Fiat',
        128 => 'Fiat',
        130 => 'Fiat',
        132 => 'Fiat',
        238 => 'Fiat',
        '500L' => 'Fiat',
        '500X' => 'Fiat',
        508 => 'Fiat',
        '900T' => 'Fiat',
        'Argenta' => 'Fiat',
        'Duna' => 'Fiat',
        'Qubo' => 'Fiat',
        'Ritmo' => 'Fiat',
        'Strada' => 'Fiat',
        'X 1/9' => 'Fiat',
        'Aspire' => 'Ford',
        'B-MAX' => 'Ford',
        'Bronco-II' => 'Ford',
        'Consul' => 'Ford',
        'Custom' => 'Ford',
        'Escort (North America)' => 'Ford',
        'Everest' => 'Ford',
        'Fairlane' => 'Ford',
        'Fairmont' => 'Ford',
        'Falcon' => 'Ford',
        'Festiva' => 'Ford',
        'Fiesta ST' => 'Ford',
        'Five Hundred' => 'Ford',
        'Focus (North America)' => 'Ford',
        'Focus RS' => 'Ford',
        'Focus ST' => 'Ford',
        'Freda' => 'Ford',
        'Fusion (North America)' => 'Ford',
        'Galaxie' => 'Ford',
        'GPA' => 'Ford',
        'Granada (North America)' => 'Ford',
        'GT40' => 'Ford',
        'Ikon' => 'Ford',
        'Ixion' => 'Ford',
        'LTD Crown Victoria' => 'Ford',
        'M151' => 'Ford',
        'Mainline' => 'Ford',
        'Model A' => 'Ford',
        'Model T' => 'Ford',
        'Mondeo ST' => 'Ford',
        'Ranchero' => 'Ford',
        'Ranger (North America)' => 'Ford',
        'S-MAX' => 'Ford',
        'Spectron' => 'Ford',
        'Taurus X' => 'Ford',
        'Telstar' => 'Ford',
        'Territory' => 'Ford',
        'Torino' => 'Ford',
        'Tourneo Courier' => 'Ford',
        'V8' => 'Ford',
        'Zephyr' => 'Ford',
        'Midi' => 'Foton',
        'Sauvana' => 'Foton',
        'Tunland' => 'Foton',
        'Atlas' => 'Geely',
        'Beauty Leopard' => 'Geely',
        'CK (Otaka)' => 'Geely',
        'Emgrand 7' => 'Geely',
        'Emgrand EC8' => 'Geely',
        'Emgrand GT' => 'Geely',
        'GC9' => 'Geely',
        'Haoqing' => 'Geely',
        'LC (Panda)' => 'Geely',
        'LC (Panda) Cross' => 'Geely',
        'MK Cross' => 'Geely',
        'MR' => 'Geely',
        'SC7' => 'Geely',
        'TX4' => 'Geely',
        'FC (Vision)' => 'Geely',
        'G70' => 'Genesis',
        'G80' => 'Genesis',
        'G90' => 'Genesis',
        'Sonoma' => 'GMC',
        'Syclone' => 'GMC',
        'Vandura' => 'GMC',
        'Cowry (V80)' => 'Great Wall',
        'Hover M1 (Peri 4x4)' => 'Great Wall',
        'Sing RUV' => 'Great Wall',
        'Voleex C10 (Phenom)' => 'Great Wall',
        'Voleex C30' => 'Great Wall',
        'Saibao' => 'Hafei',
        'Sigma' => 'Hafei',
        2 => 'Haima',
        'Family F7' => 'Haima',
        'M3' => 'Haima',
        'S5' => 'Haima',
        'H6 Coupe' => 'Haval',
        'Apollo' => 'Holden',
        'Barina' => 'Holden',
        'Calais' => 'Holden',
        'Caprice' => 'Holden',
        'Commodore' => 'Opel',
        'Cruze' => 'Holden',
        'Frontera' => 'Holden',
        'Jackaroo' => 'Holden',
        'Monaro' => 'Holden',
        'Rodeo' => 'Renault',
        'Statesman' => 'Holden',
        'UTE' => 'Holden',
        'Vectra' => 'Vauxhall',
        145 => 'Honda',
        'Ballade' => 'Honda',
        'Brio' => 'Honda',
        'FCX Clarity' => 'Honda',
        'Grace' => 'Honda',
        'Jade' => 'Honda',
        'N360' => 'Honda',
        'N-BOX' => 'Honda',
        'N-One' => 'Honda',
        'N-WGN' => 'Honda',
        'Odyssey (North America)' => 'Honda',
        'S500' => 'Honda',
        'S600' => 'Honda',
        'S660' => 'Honda',
        'That\'S' => 'Honda',
        'Vezel' => 'Honda',
        'Antelope' => 'HuangHai',
        'Landscape' => 'HuangHai',
        'Plutus' => 'HuangHai',
        'Deluxe Eight' => 'Hudson',
        'Super Six' => 'Opel',
        'Aslan' => 'Hyundai',
        'Azera' => 'Hyundai',
        'Click' => 'Hyundai',
        'EON' => 'Hyundai',
        'Excel' => 'Lotus',
        'H200' => 'Hyundai',
        'i30 N' => 'Hyundai',
        'IONIQ' => 'Hyundai',
        'Kona' => 'Hyundai',
        'Marcia' => 'Hyundai',
        'Maxcruz' => 'Hyundai',
        'Palisade' => 'Hyundai',
        'Scoupe' => 'Hyundai',
        'Stellar' => 'Hyundai',
        'Tuscani' => 'Hyundai',
        'Q30' => 'Infiniti',
        'Q40' => 'Infiniti',
        'Q50' => 'Infiniti',
        'Q60' => 'Infiniti',
        'Q70' => 'Infiniti',
        'QX30' => 'Infiniti',
        'QX4' => 'Infiniti',
        'QX50' => 'Infiniti',
        'QX56' => 'Infiniti',
        'QX60' => 'Infiniti',
        'QX70' => 'Infiniti',
        'QX80' => 'Infiniti',
        'Dena' => 'Iran Khodro',
        'Paykan' => 'Iran Khodro',
        'Runna' => 'Iran Khodro',
        'Sahra' => 'Iran Khodro',
        'Sarir' => 'Iran Khodro',
        'Soren' => 'Iran Khodro',
        117 => 'Isuzu',
        'Bellett' => 'Isuzu',
        'Florian' => 'Isuzu',
        'Impulse' => 'Isuzu',
        'KB' => 'Isuzu',
        'MU-7' => 'Isuzu',
        'MU-X' => 'Isuzu',
        'Stylus' => 'Isuzu',
        'TF (Pickup)' => 'Isuzu',
        'E-Pace' => 'Jaguar',
        'E-type' => 'Jaguar',
        'F-Pace' => 'Jaguar',
        'F-Type' => 'Jaguar',
        'F-Type SVR' => 'Jaguar',
        'I-Pace' => 'Jaguar',
        'Mark 2' => 'Jaguar',
        'S-Type' => 'Jaguar',
        'X-Type' => 'Jaguar',
        'XJ220' => 'Jaguar',
        'CJ' => 'Jeep',
        'Grand Wagoneer' => 'Jeep',
        'Liberty (North America)' => 'Jeep',
        'Liberty (Patriot)' => 'Jeep',
        'Capital' => 'Kia',
        'Carstar' => 'Kia',
        'Ceed GT' => 'Kia',
        'Elan' => 'Lotus',
        'Enterprise' => 'Kia',
        'K3' => 'Kia',
        'K5' => 'Kia',
        'K7' => 'Kia',
        'K9' => 'Kia',
        'Lotze' => 'Kia',
        'Mentor' => 'Kia',
        'Mohave (Borrego)' => 'Kia',
        'Niro' => 'Kia',
        'Potentia' => 'Kia',
        'Quanlima' => 'Kia',
        'Ray' => 'Kia',
        'Stonic' => 'Kia',
        'Towner' => 'Kia',
        'Visto' => 'Kia',
        'X-Trek' => 'Kia',
        '1111 Ока' => 'LADA (ВАЗ)',
        2101 => 'LADA (ВАЗ)',
        2102 => 'LADA (ВАЗ)',
        2103 => 'LADA (ВАЗ)',
        2104 => 'LADA (ВАЗ)',
        2105 => 'LADA (ВАЗ)',
        2106 => 'LADA (ВАЗ)',
        2107 => 'LADA (ВАЗ)',
        2108 => 'LADA (ВАЗ)',
        2109 => 'LADA (ВАЗ)',
        21099 => 'LADA (ВАЗ)',
        2110 => 'LADA (ВАЗ)',
        2111 => 'LADA (ВАЗ)',
        2112 => 'LADA (ВАЗ)',
        2113 => 'LADA (ВАЗ)',
        2114 => 'LADA (ВАЗ)',
        2115 => 'LADA (ВАЗ)',
        '2120 Надежда' => 'LADA (ВАЗ)',
        '2121 (4x4)' => 'LADA (ВАЗ)',
        2123 => 'LADA (ВАЗ)',
        2129 => 'LADA (ВАЗ)',
        '2131 (4x4)' => 'LADA (ВАЗ)',
        'Priora' => 'LADA (ВАЗ)',
        2328 => 'LADA (ВАЗ)',
        2329 => 'LADA (ВАЗ)',
        'EL Lada' => 'LADA (ВАЗ)',
        'Granta' => 'LADA (ВАЗ)',
        'Kalina' => 'LADA (ВАЗ)',
        'Largus' => 'LADA (ВАЗ)',
        'Revolution' => 'LADA (ВАЗ)',
        'Vesta' => 'LADA (ВАЗ)',
        'XRAY' => 'LADA (ВАЗ)',
        '350/400 GT' => 'Lamborghini',
        'Aventador' => 'Lamborghini',
        'Centenario' => 'Lamborghini',
        'Countach' => 'Lamborghini',
        'Diablo' => 'Lamborghini',
        'Egoista' => 'Lamborghini',
        'Espada' => 'Lamborghini',
        'Gallardo' => 'Lamborghini',
        'Huracán' => 'Lamborghini',
        'Islero' => 'Lamborghini',
        'Jalpa' => 'Lamborghini',
        'Jarama' => 'Lamborghini',
        'LM001' => 'Lamborghini',
        'LM002' => 'Lamborghini',
        'Miura' => 'Lamborghini',
        'Murcielago' => 'Lamborghini',
        'Reventon' => 'Lamborghini',
        'Sesto Elemento' => 'Lamborghini',
        'Silhouette' => 'Oldsmobile',
        'Urraco' => 'Lamborghini',
        'Urus' => 'Lamborghini',
        'Veneno' => 'Lamborghini',
        'A 112' => 'Lancia',
        'Appia' => 'Lancia',
        'Aurelia' => 'Lancia',
        'Beta' => 'Lancia',
        'Flaminia' => 'Lancia',
        'Flavia' => 'Lancia',
        'Fulvia' => 'Lancia',
        'Gamma' => 'Lancia',
        'Hyena' => 'Lancia',
        'Lambda' => 'Lancia',
        'Monte Carlo' => 'Lancia',
        'Rally 037' => 'Lancia',
        'Stratos' => 'Lancia',
        'Trevi' => 'Lancia',
        'Voyager' => 'Lancia',
        'Y10' => 'Lancia',
        'Zeta' => 'Lancia',
        'Series I' => 'Land Rover',
        'Series II' => 'Land Rover',
        'Series III' => 'Land Rover',
        'GS F' => 'Lexus',
        'IS F' => 'Lexus',
        'LC' => 'Lexus',
        'RC F' => 'Lexus',
        'UX' => 'Lexus',
        'Breez (520)' => 'Lifan',
        'Cebrium (720)' => 'Lifan',
        'Celliya (530)' => 'Lifan',
        'Murman' => 'Lifan',
        'Myway' => 'Lifan',
        'X70' => 'Lifan',
        'Blackwood' => 'Lincoln',
        'Capri' => 'Mercury',
        'Mark III' => 'Lincoln',
        'Mark IV' => 'Lincoln',
        'Mark VII' => 'Lincoln',
        'Mark VIII' => 'Lincoln',
        'MKC' => 'Lincoln',
        'Nautilus' => 'Lincoln',
        'Premiere' => 'Lincoln',
        '340R' => 'Lotus',
        'Eclat' => 'Lotus',
        'Elise' => 'Lotus',
        'Elite' => 'Lotus',
        'Esprit' => 'Lotus',
        'Europa' => 'Lotus',
        'Europa S' => 'Lotus',
        'Evora' => 'Lotus',
        'Exige' => 'Lotus',
        'Luxgen7 MPV' => 'Luxgen',
        'Luxgen7 SUV' => 'Luxgen',
        'Luxgen5' => 'Luxgen',
        'U6 Turbo' => 'Luxgen',
        'U7 Turbo' => 'Luxgen',
        228 => 'Maserati',
        '3200 GT' => 'Maserati',
        420 => 'Maserati',
        '4200 GT' => 'Maserati',
        'Barchetta Stradale' => 'Maserati',
        'Biturbo' => 'Maserati',
        'Bora' => 'Maserati',
        'Chubasco' => 'Maserati',
        'Ghibli' => 'Maserati',
        'GranTurismo' => 'Maserati',
        'Indy' => 'Maserati',
        'Karif' => 'Maserati',
        'Khamsin' => 'Maserati',
        'Kyalami' => 'Maserati',
        'Levante' => 'Maserati',
        'MC12' => 'Maserati',
        'Merak' => 'Maserati',
        'Mexico' => 'Maserati',
        'Quattroporte' => 'Maserati',
        'Shamal' => 'Maserati',
        'Exelero' => 'Maybach',
        1000 => 'Subaru',
        '3 MPS' => 'Mazda',
        616 => 'Mazda',
        '6 MPS' => 'Mazda',
        818 => 'Mazda',
        'AZ-1' => 'Mazda',
        'Chantez' => 'Mazda',
        'Cosmo' => 'Mazda',
        'CX-3' => 'Mazda',
        'CX-5' => 'Mazda',
        'CX-7' => 'Mazda',
        'CX-8' => 'Mazda',
        'CX-9' => 'Mazda',
        'Etude' => 'Mazda',
        'Flair' => 'Mazda',
        'Flair Crossover' => 'Mazda',
        'Navajo' => 'Mazda',
        'Proceed Marvie' => 'Mazda',
        'R360' => 'Mazda',
        'MP4-12C' => 'McLaren',
        '540C' => 'McLaren',
        '570GT' => 'McLaren',
        '570S' => 'McLaren',
        '650S' => 'McLaren',
        '675LT' => 'McLaren',
        '720S' => 'McLaren',
        'F1' => 'McLaren',
        'P1' => 'McLaren',
        '190 SL' => 'Mercedes-Benz',
        'A-klasse' => 'Mercedes-Benz',
        'A-klasse AMG' => 'Mercedes-Benz',
        'GLC Coupe AMG' => 'Mercedes-Benz',
        'AMG GT' => 'Mercedes-Benz',
        'B-klasse' => 'Mercedes-Benz',
        'C-klasse' => 'Mercedes-Benz',
        'C-klasse AMG' => 'Mercedes-Benz',
        'CL-klasse' => 'Mercedes-Benz',
        'CL-klasse AMG' => 'Mercedes-Benz',
        'CLA-klasse' => 'Mercedes-Benz',
        'CLA-klasse AMG' => 'Mercedes-Benz',
        'CLC-klasse' => 'Mercedes-Benz',
        'CLK-klasse' => 'Mercedes-Benz',
        'CLK-klasse AMG' => 'Mercedes-Benz',
        'CLS-klasse' => 'Mercedes-Benz',
        'CLS-klasse AMG' => 'Mercedes-Benz',
        'E-klasse' => 'Mercedes-Benz',
        'E-klasse AMG' => 'Mercedes-Benz',
        'G-klasse' => 'Mercedes-Benz',
        'G-klasse AMG' => 'Mercedes-Benz',
        'G-klasse AMG 6x6' => 'Mercedes-Benz',
        'GL-klasse' => 'Mercedes-Benz',
        'GL-klasse AMG' => 'Mercedes-Benz',
        'GLA-klasse' => 'Mercedes-Benz',
        'GLA-klasse AMG' => 'Mercedes-Benz',
        'GLC Coupe' => 'Mercedes-Benz',
        'GLC' => 'Mercedes-Benz',
        'GLC AMG' => 'Mercedes-Benz',
        'GLE' => 'Mercedes-Benz',
        'GLE AMG' => 'Mercedes-Benz',
        'GLE Coupe' => 'Mercedes-Benz',
        'GLE Coupe AMG' => 'Mercedes-Benz',
        'GLK-klasse' => 'Mercedes-Benz',
        'GLS-klasse' => 'Mercedes-Benz',
        'GLS-klasse AMG' => 'Mercedes-Benz',
        'M-klasse' => 'Mercedes-Benz',
        'M-klasse AMG' => 'Mercedes-Benz',
        'Maybach G 650 Landaulet' => 'Mercedes-Benz',
        'R-klasse' => 'Mercedes-Benz',
        'R-klasse AMG' => 'Mercedes-Benz',
        'Maybach S-klasse' => 'Mercedes-Benz',
        'S-klasse' => 'Mercedes-Benz',
        'S-klasse AMG' => 'Mercedes-Benz',
        'SL-klasse' => 'Mercedes-Benz',
        'SL-klasse AMG' => 'Mercedes-Benz',
        'SLC-klasse' => 'Mercedes-Benz',
        'SLC-klasse AMG' => 'Mercedes-Benz',
        'SLK-klasse' => 'Mercedes-Benz',
        'SLK-klasse AMG' => 'Mercedes-Benz',
        'SLR McLaren' => 'Mercedes-Benz',
        'V-klasse' => 'Mercedes-Benz',
        'Viano' => 'Mercedes-Benz',
        'Vito' => 'Mercedes-Benz',
        'W100' => 'Mercedes-Benz',
        'W105' => 'Mercedes-Benz',
        'W108' => 'Mercedes-Benz',
        'W110' => 'Mercedes-Benz',
        'W111' => 'Mercedes-Benz',
        'W114' => 'Mercedes-Benz',
        'W115' => 'Mercedes-Benz',
        'W120' => 'Mercedes-Benz',
        'W121' => 'Mercedes-Benz',
        'W123' => 'Mercedes-Benz',
        'W124' => 'Mercedes-Benz',
        'W128' => 'Mercedes-Benz',
        'W136' => 'Mercedes-Benz',
        'W142' => 'Mercedes-Benz',
        'W186' => 'Mercedes-Benz',
        'W188' => 'Mercedes-Benz',
        'W189' => 'Mercedes-Benz',
        '190 (W201)' => 'Mercedes-Benz',
        'W29' => 'Mercedes-Benz',
        'X-klasse' => 'Mercedes-Benz',
        'Marauder' => 'Mercury',
        'Marquis' => 'Mercury',
        'Mystique' => 'Mercury',
        'Tracer' => 'Mercury',
        3 => 'MG',
        350 => 'MG',
        5 => 'MG',
        750 => 'MG',
        'F' => 'MG',
        'Maestro' => 'MG',
        'Metro' => 'Rover',
        'MGA' => 'MG',
        'MGB' => 'MG',
        'Montego' => 'MG',
        'RV8' => 'MG',
        'Xpower SV' => 'MG',
        500 => 'Mitsubishi',
        'Attrage' => 'Mitsubishi',
        'Celeste' => 'Mitsubishi',
        'Eclipse Cross' => 'Mitsubishi',
        'eK Active' => 'Mitsubishi',
        'eK Classic' => 'Mitsubishi',
        'eK Custom' => 'Mitsubishi',
        'eK Space' => 'Mitsubishi',
        'eK Sport' => 'Mitsubishi',
        'eK Wagon' => 'Mitsubishi',
        'Freeca' => 'Mitsubishi',
        'Jeep J' => 'Mitsubishi',
        'L400' => 'Mitsubishi',
        'Lancer Ralliart' => 'Mitsubishi',
        'Savrin' => 'Mitsubishi',
        'Xpander' => 'Mitsubishi',
        '3 Wheeler' => 'Morgan',
        '4/4' => 'Morgan',
        '4 Seater' => 'Morgan',
        'Aero 8' => 'Morgan',
        'Aero Coupe' => 'Morgan',
        'Aero SuperSports' => 'Morgan',
        'AeroMax' => 'Morgan',
        'Plus 4' => 'Morgan',
        'Plus 8' => 'Morgan',
        'Roadster' => 'Tesla',
        '240SX' => 'Nissan',
        'Auster' => 'Nissan',
        'NV100 Clipper' => 'Nissan',
        'Clipper Rio' => 'Nissan',
        'Dayz' => 'Nissan',
        'Exa' => 'Nissan',
        'Juke Nismo' => 'Nissan',
        'Kicks' => 'Nissan',
        'Langley' => 'Nissan',
        'Leaf' => 'Nissan',
        'Liberta Villa' => 'Nissan',
        'Livina' => 'Nissan',
        'Navara (Frontier)' => 'Nissan',
        'NV200' => 'Nissan',
        'NV350 Caravan' => 'Nissan',
        'NX Coupe' => 'Nissan',
        'Pixo' => 'Nissan',
        'R\'nessa' => 'Nissan',
        'Achieva' => 'Oldsmobile',
        'Alero' => 'Oldsmobile',
        'Aurora' => 'Oldsmobile',
        'Bravada' => 'Oldsmobile',
        'Cutlass' => 'Oldsmobile',
        'Cutlass Calais' => 'Oldsmobile',
        'Cutlass Ciera' => 'Oldsmobile',
        'Cutlass Supreme' => 'Oldsmobile',
        'Eighty-Eight' => 'Oldsmobile',
        'Firenza' => 'Vauxhall',
        'Intrigue' => 'Oldsmobile',
        'Ninety-Eight' => 'Oldsmobile',
        'Series 60' => 'Oldsmobile',
        'Series 70' => 'Oldsmobile',
        'Starfire' => 'Oldsmobile',
        'Toronado' => 'Oldsmobile',
        'Vista Cruiser' => 'Oldsmobile',
        'Adam' => 'Vauxhall',
        'Astra OPC' => 'Opel',
        'Cascada' => 'Opel',
        'Corsa OPC' => 'Opel',
        'Crossland X' => 'Opel',
        'Grandland X' => 'Opel',
        'Insignia OPC' => 'Opel',
        'Kapitan' => 'Opel',
        'Karl' => 'Opel',
        'Meriva OPC' => 'Opel',
        'Olympia' => 'Opel',
        'P4' => 'Rover',
        'Vectra OPC' => 'Opel',
        'Zafira OPC' => 'Opel',
        104 => 'Peugeot',
        108 => 'Peugeot',
        201 => 'Peugeot',
        202 => 'Peugeot',
        203 => 'Peugeot',
        204 => 'Peugeot',
        '205 GTi' => 'Peugeot',
        '208 GTi' => 'Peugeot',
        304 => 'Peugeot',
        '308 GTi' => 'Peugeot',
        402 => 'Peugeot',
        403 => 'Peugeot',
        404 => 'Peugeot',
        504 => 'Peugeot',
        505 => 'Peugeot',
        604 => 'Peugeot',
        'iOn' => 'Peugeot',
        'Barracuda' => 'Plymouth',
        'Caravelle' => 'Renault',
        'Fury' => 'Plymouth',
        'Reliant' => 'Plymouth',
        'Road Runner' => 'Plymouth',
        'Turismo' => 'Plymouth',
        'Valiant' => 'Plymouth',
        'Aztek' => 'Pontiac',
        'Catalina' => 'Pontiac',
        'G4' => 'Pontiac',
        'Laurentian' => 'Pontiac',
        'LeMans' => 'Pontiac',
        'Parisienne' => 'Pontiac',
        'Phoenix' => 'Pontiac',
        'Sunbird' => 'Pontiac',
        'Tempest' => 'Pontiac',
        'Torpedo' => 'Pontiac',
        356 => 'Porsche',
        '718 Boxster' => 'Porsche',
        '718 Cayman' => 'Porsche',
        '911 GT2' => 'Porsche',
        '911 GT3' => 'Porsche',
        '911 R' => 'Porsche',
        912 => 'Porsche',
        914 => 'Porsche',
        '918 Spyder' => 'Porsche',
        959 => 'Porsche',
        'Carrera GT' => 'Porsche',
        'Cayman GT4' => 'Porsche',
        'Arena' => 'Proton',
        'Exora' => 'Proton',
        'Gen-2' => 'Proton',
        'Inspira' => 'Proton',
        'Juara' => 'Proton',
        'Perdana' => 'Proton',
        'Persona' => 'Proton',
        'Preve' => 'Proton',
        'Putra' => 'Proton',
        'Saga' => 'Proton',
        'Satria' => 'Proton',
        'Savvy' => 'Proton',
        'Tiara' => 'Proton',
        'Waja' => 'Proton',
        'Wira (400 Series)' => 'Proton',
        'GTB' => 'Puma',
        'GTE' => 'Puma',
        'Nexia R3' => 'Ravon',
        10 => 'Renault',
        12 => 'Renault',
        14 => 'Rover',
        15 => 'Renault',
        16 => 'Renault',
        17 => 'Renault',
        18 => 'Renault',
        20 => 'Renault',
        30 => 'Renault',
        4 => 'Renault',
        '4CV' => 'Renault',
        8 => 'Renault',
        'Alaskan' => 'Renault',
        'Avantime' => 'Renault',
        'Clio RS' => 'Renault',
        'Clio V6' => 'Renault',
        'Dauphine' => 'Renault',
        'Floride' => 'Renault',
        'Fregate' => 'Renault',
        'Fuego' => 'Renault',
        'Kadjar' => 'Renault',
        'KWID' => 'Renault',
        'Megane RS' => 'Renault',
        'Sandero RS' => 'Renault',
        'Sport Spider' => 'Renault',
        'Talisman' => 'Renault',
        'Twizy' => 'Renault',
        'Vivastella' => 'Renault',
        'ZOE' => 'Renault',
        '20/25' => 'Rolls-Royce',
        'Camargue' => 'Rolls-Royce',
        'Dawn' => 'Rolls-Royce',
        'Park Ward' => 'Rolls-Royce',
        'Silver Cloud' => 'Rolls-Royce',
        'Silver Ghost' => 'Rolls-Royce',
        'Silver Shadow' => 'Rolls-Royce',
        'Silver Spur' => 'Rolls-Royce',
        'Silver Wraith' => 'Rolls-Royce',
        100 => 'Rover',
        'P3' => 'Rover',
        'P6' => 'Rover',
        'SD1' => 'Rover',
        90 => 'Saab',
        93 => 'Saab',
        95 => 'Saab',
        96 => 'Saab',
        'Sonett' => 'Saab',
        'LS' => 'Saturn',
        'LW' => 'Saturn',
        'SC' => 'Saturn',
        'SL' => 'Saturn',
        'SW' => 'Saturn',
        'iM' => 'Scion',
        133 => 'SEAT',
        'Arona' => 'SEAT',
        'Ateca' => 'SEAT',
        'Fura' => 'SEAT',
        'Ibiza Cupra' => 'SEAT',
        'Inca' => 'SEAT',
        'Leon Cupra' => 'SEAT',
        'Malaga' => 'SEAT',
        'Marbella' => 'SEAT',
        'Mii' => 'SEAT',
        'Ronda' => 'SEAT',
        'Tarraco' => 'SEAT',
        '100 Series' => 'Skoda',
        '105, 120' => 'Skoda',
        1200 => 'Skoda',
        'Citigo' => 'Skoda',
        'Fabia RS' => 'Skoda',
        'Forman' => 'Skoda',
        'Karoq' => 'Skoda',
        'Kodiaq RS' => 'Skoda',
        'Octavia RS' => 'Skoda',
        'Popular' => 'Skoda',
        'Kallista' => 'SsangYong',
        'Korando Family' => 'SsangYong',
        'Korando Sports' => 'SsangYong',
        'Korando Turismo' => 'SsangYong',
        'Nomad' => 'SsangYong',
        'Tivoli' => 'SsangYong',
        'XLV' => 'SsangYong',
        'Ascent' => 'Subaru',
        'Bighorn' => 'Subaru',
        'Bistro' => 'Subaru',
        'Brat' => 'Subaru',
        'SVX' => 'Subaru',
        'WRX' => 'Subaru',
        'WRX STi' => 'Subaru',
        'XT' => 'Subaru',
        'APV' => 'Suzuki',
        'Carry' => 'Suzuki',
        'Celerio' => 'Suzuki',
        'Ertiga' => 'Suzuki',
        'Hustler' => 'Suzuki',
        'Spacia' => 'Suzuki',
        'Wagon R+' => 'Suzuki',
        'XL7' => 'Suzuki',
        'Model 3' => 'Tesla',
        '2000GT' => 'Toyota',
        '4Runner' => 'Toyota',
        'Aqua' => 'Toyota',
        'Aurion' => 'Toyota',
        'Avanza' => 'Toyota',
        'Blizzard' => 'Toyota',
        'Camry Solara' => 'Toyota',
        'Comfort' => 'Toyota',
        'Cressida' => 'Toyota',
        'Esquire' => 'Toyota',
        'Etios' => 'Toyota',
        'Hilux' => 'Toyota',
        'Innova' => 'Toyota',
        'MasterAce Surf' => 'Toyota',
        'Mirai' => 'Toyota',
        'Model F' => 'Toyota',
        'Origin' => 'Toyota',
        'Pixis Epoch' => 'Toyota',
        'Pixis Mega' => 'Toyota',
        'Pixis Space' => 'Toyota',
        'Prius Alpha' => 'Toyota',
        'Prius c' => 'Toyota',
        'Prius v (+)' => 'Toyota',
        'ProAce' => 'Toyota',
        'Publica' => 'Toyota',
        'RAV 4' => 'Toyota',
        'RegiusAce' => 'Toyota',
        'Soluna' => 'Toyota',
        'Spade' => 'Toyota',
        'Sports 800' => 'Toyota',
        'Verso-S' => 'Toyota',
        'Vios' => 'Toyota',
        'WiLL' => 'Toyota',
        'Carlton' => 'Vauxhall',
        'Ventora' => 'Vauxhall',
        'Viceroy' => 'Vauxhall',
        'Victor' => 'Vauxhall',
        'Viva' => 'Vauxhall',
        'VXR8' => 'Vauxhall',
        181 => 'Volkswagen',
        'Derby' => 'Volkswagen',
        'Golf Country' => 'Volkswagen',
        'Golf GTI' => 'Volkswagen',
        'Golf R' => 'Volkswagen',
        'Golf R32' => 'Volkswagen',
        'Golf Sportsvan' => 'Volkswagen',
        'K70' => 'Volkswagen',
        'Karmann-Ghia' => 'Volkswagen',
        'Lupo GTI' => 'Volkswagen',
        'Passat (North America)' => 'Volkswagen',
        'Polo GTI' => 'Volkswagen',
        'Polo R WRC' => 'Volkswagen',
        'Quantum' => 'Volkswagen',
        'Scirocco R' => 'Volkswagen',
        'T-Roc' => 'Volkswagen',
        'Teramont' => 'Volkswagen',
        'Type 1' => 'Volkswagen',
        'Type 2' => 'Volkswagen',
        'Type 3' => 'Volkswagen',
        'Type 4' => 'Volkswagen',
        'up!' => 'Volkswagen',
        'XL1' => 'Volkswagen',
        '120 Series' => 'Volvo',
        '140 Series' => 'Volvo',
        164 => 'Volvo',
        '240 Series' => 'Volvo',
        '260 Series' => 'Volvo',
        '300 Series' => 'Volvo',
        66 => 'Volvo',
        780 => 'Volvo',
        'Laplander' => 'Volvo',
        'P1800' => 'Volvo',
        'P1900' => 'Volvo',
        'S60 Cross Country' => 'Volvo',
        'V40 Cross Country' => 'Volvo',
        'V60 Cross Country' => 'Volvo',
        'V90' => 'Volvo',
        'V90 Cross Country' => 'Volvo',
        '2-Series Gran Tourer' => 'BMW',
        '3-Series Gran Turismo' => 'BMW',
        '5-Series Gran Turismo' => 'BMW',
        'Delta' => 'Daihatsu',
        'Mebius' => 'Daihatsu',
        'Mira Avy' => 'Daihatsu',
        'H1' => 'Hyundai',
        'Cerato Koup' => 'Kia',
        'CT200h' => 'Lexus',
        'Flairwagon' => 'Mazda',
        'Mazda3 MPS' => 'Mazda',
        'Savanna RX-7' => 'Mazda',
        'Mirage Dingo' => 'Mitsubishi',
        'DAYZ Roox' => 'Nissan',
        'NT100 Clipper' => 'Nissan',
        'Terrano II' => 'Nissan',
        'Astra Family' => 'Opel',
        'Astra GTC' => 'Opel',
        'Camry Prominent' => 'Toyota',
        'Corona SF' => 'Toyota',
        'Pixis Truck' => 'Toyota',
        'Pixis Van' => 'Toyota',
        'Prius a' => 'Toyota',
        'Prius PHV' => 'Toyota',
        'Partner Tepee' => 'Peugeot',
    );
}

?>