<table class="table">
    <thead>
        <th>Марка</th>
        <th>Модель</th>
        <th>Код кузова</th>
        <th>Комплектация</th>
        <th>Тип кузова</th>
        <th>Период выпуска</th>
        <th>Тип ДВС</th>
        <th>Объём ДВС, л</th>
        <th>Мощность ДВС, л.с.</th>
        <th>Тип КПП</th>
        <th>Тип привода</th>
    </thead>
    <tbody>
        @foreach ($results as $result)
            <tr>
                <td>{{$result->mark}}</td>
                <td>{{$result->model}}</td>
                <td>{{$result->body_code}}</td>
                <td>{{$result->model_name}}</td>
                <td>{{$result->body_type}}</td>
                <td>{{$result->year_from}} - {{$result->year_to}}</td>
                <td>{{$result->engine_type}}</td>
                <td>{{$result->volume}}</td>
                <td>{{$result->horse_power}}</td>
                <td>{{$result->box_type}}</td>
                <td>{{$result->unit_type}}</td>
                <td></td>
            </tr>
        @endforeach
    </tbody>
</table>