@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="">
                <div class="panel panel-default">
                    <div class="panel-heading">API Settings</div>

                    <div class="panel-body">
                        <form action="" method="POST">
                            {{ csrf_field() }}
                            <input class="form-control" value="{{Auth::user()->crm_email}}" name="crm_email" placeholder="CRM Email">
                            <input class="form-control" value="{{Auth::user()->crm_key}}" name="api_key" placeholder="API key" style="margin-top: 10px;">
                            <input type="submit" class="btn btn-success" style="margin-top: 10px;" value="Сохранить">
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
