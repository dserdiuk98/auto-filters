@extends('layouts.app')

@section('content')
    <div class="container-full">
        <div class="row">
            <div class="">
                <div class="panel panel-default">
                    <div class="panel-heading">Dashboard</div>
                    <div class="panel-body">
                        <filter-component :params="{{ json_encode($params) }}"
                                          :is_admin="{{$is_admin}}"
                                          :search_base="{{ $search_base }}"></filter-component>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
