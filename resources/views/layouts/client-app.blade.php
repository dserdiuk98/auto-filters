<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container" style="text-align: center;">
                <a class="page-title" href="{{ url('/') }}">
                    КАТАЛОГ
                </a>
            </div>
        </nav>

        @yield('content')
    </div>
    <!-- Модальное окно -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <script>var amo_forms_params = {
                            "id": 445701,
                            "hash": "2d75be5c59e0ac27bbe7027915b29acf",
                            "locale": "ru"
                        };</script>
                    <script id="amoforms_script" async="async" charset="utf-8"
                            src="https://forms.amocrm.ru/forms/assets/js/amoforms.js"></script>
                </div>
            </div>
        </div>
    </div>

    <div id="footer">
        <div>
            <b>Не нашли подходящей комплектации?</b> Оставляйте заявку. Поможем в подборе коробки передач для вашего авто.

            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
               Подобрать
            </button>

        </div>
    </div>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
