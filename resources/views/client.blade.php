@extends('layouts.client-app')

@section('content')
    <div class="container-full">
        <div class="row">
            <div class="">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <client-filter-component :params="{{ json_encode($params) }}"
                                                 :selectparams="{{json_encode($selectParams)}}">
                        </client-filter-component>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
